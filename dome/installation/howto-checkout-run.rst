How to check-out and run DOME code
===================================

This article outlines how to check out DOME source code from DMC repository, to compile and to run the code. 

Prerequisites:

 * User accounts already created in Digital Commons Cloud collaboration tool (atlassina.net) and source management tool repository (https://www.bitbucket.org) before the following operations.
 * The following tools need to be installed: JDK (Java Development Kit),  Git (http://git-scm.com/download/), Ant ( http://ant.apache.org/), optionally Eclipse (http://www.eclipse.org/home/index.php).
 
Step-by-step guide
-------------------

1. Login to Atlassian Cloud.

   From a web browser,  type the following URL: (https://digitalmfgcommons.atlassian.net/)
   When a login screen appears, type your atlassiant.net user name and password:
   
   .. image:: _static/howto-checkout/step1-login.png

   
2. Go to BitBucket in Atlassian (https://bitbucket.org/DigitalMfgCommons/dome)
   After login, click the option list on the top left corner to go into BitBucket. Note: if you have not sign in to BitBucket, and remember the user name and password on the machine, you will be ask for BitBucket user name and password.
   
   .. image:: _static/howto-checkout/step2-menu.png
   

   After go into BitBucket, you can see all the available projects listed similar to below:
   
   .. image:: _static/howto-checkout/step2-bitbucket.png
   

   There maybe more projects listed for individual users, but you should have “DigitalMfgCommons / DOME" for the DOME source code repository. Click on it, the following page will shown with Clone listed on the left as one of the Actions:
   
   .. image:: _static/howto-checkout/step2-clone.png
   

   Note: If you do not see the Actions panel,  your configuration may have Clone in the fly-out action menu from the ellipse (...) as shown here:
   
   .. image:: _static/howto-checkout/step2-create-branch.png
   

   Click “Clone”, the system will pop up a small page generate Git script needed to check out the source code from BitBucket, for example:
   ::
   
      git clone https://BitBucketName@bitbucket.org/DigitalMfgCommons/dome.git
   
   Hit Ctrl+C to copy the string into buffer.
   
3. If you use Windows system, open a command line window, create a directory that you would like to check out the project.  Type or paste (from C:\ icon from top left of the window) the git script you copied from the previous step to checked out code from BitBucket.   When the checkout process finishes, a message similar to below will shown:
   
   .. image:: _static/howto-checkout/step3-git-clone.png
   
   Note 1: You can perform same operations if you use Linux system.
   
   Note 2: You need to install Git before this step.  Please refer to the prerequisites.
   
   Note 3: If you access the internet from within a firewall through proxy server, you need to obtain proxy server and port from your network system administrator, and use the following two steps to set up proxy server for Git so that it can check out code from internet.  On Windows system, use system tool to add environment variable http_proxy (for example, refer to: http://best-windows.vlaurie.com/environment-variables.html):
   ::
      
	  http_proxy=GRC_Proxy_Server:port

   After the variable is defined, use the following command to define proxy server for git:
   ::
      
	  git config --global http.proxy %name of the proxy%

   To get proxy server and port, you can either ask system administrator, or go to Internet Explorer, then go to Tool–> Internet Options --> Connections --> Lan Setting.  From there, you can find the proxy server and port.  Or you can open the proxy script to find the main proxy server.  You can use similar commands to set up proxy server for git on Linux.

   Note 4: Use your Bitbucket username before @bitbucket.org.
   
   Note 5: Make sure you have permission to access Bitbucket  before do git clone command.      
   Now enter the dome directory
   ::
      
	  cd dome

4. After check out, you should see directory structure similar to that listed below with an Ant project build file build.xml included:
   
   .. image:: _static/howto-checkout/step4-dir-structure.png
   
   
5. (Option 1) You can use Ant tool command line window to build the DOME client and server.
   If you run ant in a command-line window, it should look something like the following in Windows:
   
   .. image:: _static/howto-checkout/step5-run-ant.png
   
   You need to use the following command to run Ant target:
   * To clean any old compiled code or residuals:
   
   ::
      
	  ant clean
	  
   * To compile and package for distribution (note: this step may take a while, wait until system outputs ant build successful) :
   
   ::
      
	  ant dist
	  
   * To run DOME server (after the server is running, leave the command line window open):
   
   ::
      
	  ant run-server
	  
   * To run DOME client:
   
   ::
      
	  ant run-client
	  
6. (Option 2) Build/run DOME server/client in Eclipse environment. See Development Environment Setup.
7. (Option 3) Build/run DOME server/client in Eclipse environment - git repository already cloned.
   To create a project using the checked out code and Ant build file, use Eclipse main menu:  File --> New --> Project -> Java Project.
   Uncheck the Use default location and browse to the directory where you cloned the repository.
   
   .. image:: _static/howto-checkout/step7-new-project.jpg
   
   
   
   .. attention::
      Earlier versions suggested importing from Existing Ant Buildfile, but this does not work as well because the classpath and run directory end up in your workspace instead of the code directory.

   After the project is imported to to Eclipse, if you have Ant tool in Eclipse environment, you can see that the build.xml in DOME project is marked as an Ant file.  With that, right click build.xml, and choose Run As --> Ant Build…, all the available targets are listed on the right panel. These will get saved as "External Tools Configurations".
   
   .. image:: _static/howto-checkout/step7-edit-build-config.png
   
   You can use this to build distributable packages, as well as to run the DOME server and client as explained below:
   
   * clean – clean and initialization of the system
   * dist – compile and build distribution packages for both client and server
   * run-server – run DOME server project.   From console you can see the following information:
     .. image:: _static/howto-checkout/step7-run-server.png
   * run-client – after you run client the following message will appear in Eclipse console, and the DOME interface will appear.   From here on, you can start to learn DOME model with the existing examples, or you can build your own models.
     .. image:: _static/howto-checkout/step7-run-client.png

	 
   .. attention::
      You will need multiple Ant configurations to run both client and server from the same Eclipse workspace since both will remain running. A best practice is create at least 3 or 4 configurations:
   
      * build - has clean and dist
      * run-client
      * run-server
      * test
   
      The copyright notice might appear behind your Eclipse window - if nothing seems to be happening after you start the client, minimize your Eclipse window.
      Make sure you have started the server before trying to login.
   
   Example External Tools Configurations:
   
   .. image:: _static/howto-checkout/step7-example-configs.JPG
