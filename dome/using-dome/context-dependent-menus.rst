Context Dependent Menus
========================

Context dependent menus are used throughout DOME3. Context dependent menus change according to the type of window that is selected (or in focus). Context dependent menus will always be in bold and placed between separators in the overall application menu bar.
The figure below shows an example of the menu bar when a procedural relation GUI is in focus. Procedural relations may be added when creating DOME models.

.. image:: _static/dome-menus.png