Using DOME3
=======================
Using DOME3 is divided into four separate documents, one for each of the use-mode applications. A number of tutorial examples are also provided.
To begin: 
Start the DOME3 application installed on your computer. This will execute an integrated environment that wraps the DOME suite. A DOME3 menu bar will appear at the top of your screen.

.. image:: _static/dome-window.png

In order to avoid being confused when you start to build models or projects, it is important to understand the difference between an actual object and viewing a reference to an object. You may also want to review the general behavior of the build mode GUI environment. 

.. toctree::
   :titlesonly:

   window-management.rst
   context-dependent-menus.rst
   build-mode/index
   deploy-mode.rst
   run-mode.rst
   server-mode.rst


