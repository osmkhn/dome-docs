Intermediates Filter
=======================

The intermediates filter searches the model scope in which it is applied and creates references to all intermediate parameters that it finds. Intermediate parameters are dependent parameters driven by the output of relations, but they are also inputs to other relationships in the model. Thus, the value of their data may not be changed directly by model users. 
The filter is useful for determining what intermediate variables are in a model. The filter GUI displays its contents using the list visualization view of the model GUI in which it resides. 