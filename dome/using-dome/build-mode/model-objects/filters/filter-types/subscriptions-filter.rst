Subscriptions Filter
==============================

The subscriptions filter searches the model scope in which it is applied and creates references to all interface objects that have been subscribed to from remote models. The filter GUI displays its contents using the list visualization view of the model GUI in which it resides. 
The filter is useful for determining what remote subscriptions are in a model.