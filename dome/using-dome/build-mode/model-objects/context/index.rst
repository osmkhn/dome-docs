Building Context
==================

Context objects are used to organize models and references to all model objects are allowed to be placed within it. Context support documentation. Typically all building activities that the given model supports (e.g., adding and deleting objects, mapping, etc.) are available within a context.

TODO: Tables



.. toctree::
   :titlesonly:

   context-gui.rst
