Context GUI
===============

The context GUI is shown below with its definition tab selected. The GUI is very similar to the DOME Model GUI. 

 * The name field is used to edit the context name.
 * The left arrow (under the name label) is used to change scope in the visualization window.
 * The text box (containedContext) shows the name of the scope at which the model is being viewed. This context is contained within the myContext.
 * The visualization combination box is set to determine how the objects are visualized. The same visualizations as are available in the main model GUI will be available with the context visualization area. The DOME model list visualization is shown below.
 * The up/down arrows on the right are used to re-order selected objects within the context.
 * The documentation tab is used to access the context's documentation editor.

ToDo: update screen shot: context GUI.gif showing context named containedContext (have context within and be tunneled down.
 