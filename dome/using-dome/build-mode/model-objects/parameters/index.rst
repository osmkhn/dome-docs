Building Parameters
=====================

Parameters are generic shells for data that support a number of different data types. Parameters are added to models using add menu associated with model definition tabs. Parameters available for each type of model are listed in the menu bar documentation for the given model type.

All model add menus are structured so that parameters can be added with a specified data type. However, one can change a parameter's data type after if has been added to a model using its data type chooser. 

A parameter may be defined as a constant, meaning that its data value may not be changed during run time. Additionally, constraints may be placed on data type values. Parameters also support documentation.

.. image:: _static/matrixParameter.gif



.. toctree::
   :titlesonly:

   parameter-gui.rst
   parameter-data-types/index
   data-type-chooser/index
   units/index
   constraints.rst
   conversion-and-mapping-compatibility.rst
   examples.rst
