Units Chooser GUI
==============================

The unit chooser is used to change the unit assigned to a data type. The unit chooser GUI is opened by selecting the change unit... option in a data type's unit combination box.  The unit chooser, shown below, is operated working from left to right. The desired dimension for the data type is selected in the left-most column. Only one item can be chosen from this list.  Then, unit options for the chosen dimension will appear in the units column on the right. One can filter the list of unit options according to measurement system using the combination box below the column.

.. image:: _static/unitChooser.gif
