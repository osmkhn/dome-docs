String
========

The String data type is supports a single line of text characters of any length. Strings support a number of methods and operators that can be used in relations. 
The string editor panel, which appears in the parameter GUI definition tab, is used to set the content of the string and also to define constraints on the data type.

.. image:: _static/string.gif

It is also possible to set a string's content using the value column of a model's list/tree-table view. The example below is in a DOME model.

.. image:: _static/stringList.gif

.. toctree::
   :titlesonly:

   methods-and-operators/index
   string-constraints.rst

