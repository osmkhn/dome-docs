String Methods
==============================

The String data type supports supports an equal method and a duplication method. The copyFrom method should be used (not the = operator) only if you require internal relation parameters to maintain types matching the relation interface parameters.
A summary of other String data type methods, operators and functions are on the methods and operators page.

TODO: Tables