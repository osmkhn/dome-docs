Integer Number
=================

The integer number is a data type supporting a single positive or negative whole number of up to 9 significant figures. Integer numbers also support units. Integers support a number of methods and operators that may be used to manipulate their value in relations. 
The integer editor panel (shown below), appears in the parameter GUI definition tab and is used to set the value. When you make a value change the background will show stale color until the change is committed. If you try to commit an invalid value, the background of the value field will maintain the stale color, indicating that the change has not been accepted.  The unit combination box on the right displays the unit assigned to integer. The change... option in the combination box allows you to change the value's units. The constraints button is used to define constraints on the integer value.

.. image:: _static/integer.gif

It is also possible to set the integer value and unit using the value column in a model's list/tree-table view. The example below is in a DOME model. When you click on the value cell the editor, a value editing field and a unit combination box becomes available.

.. image:: _static/integerList.gif

.. toctree::
   :titlesonly:

   methods-and-operators/index
   integer-constraints.rst
