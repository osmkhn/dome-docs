Integer Methods and Operators
==============================

Integer number methods and operators are used to write Python expressions that manipulate parameters in relations that support custom body definitions. A brief summary is provided on this page while more detailed explanations are available through the more details links. 
An integer parameter is considered true if its value is non-zero, false if its value is zero. 
For other data type methods and operators see the method and operators page.

Methods:
TODO: Add Tables

Math Operators:

Math Functions:

Type Conversions:

Compirators:




.. toctree::
   :titlesonly:

   integer-methods.rst
   integer-math-operators.rst
   integer-math-functions.rst
   integer-conversions.rst
   integer-comparitors.rst
