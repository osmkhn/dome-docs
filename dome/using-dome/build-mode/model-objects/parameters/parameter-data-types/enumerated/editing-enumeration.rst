Enumeration Editor
==============================

The enumeration editor is used to add or remove elements from the enumeration. It is opened using the edit button on the enumeration data type GUI. 
When an element is added to the enumeration, the value of the new element will have the type specified by the type combination box. Integer, real, Boolean, and string values are supported. These are Java primitive types, not DOME3 parameters. Each element in the list must be given a unique name. Any quantity can be assigned to the enumeration's value.

.. image:: _static/enumerationEdit.gif
