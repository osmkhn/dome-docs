Enumerated
============

The Enumerated data type is used for lists of name/value pairs. Enumerated data types support a number of methods and functions that can be used to in relations. Enumeration elements are indexed starting at zero.
The Enumerated data panel, which appears in the parameter GUI definition tab, is used to select an element from a list of name/value pairs. The edit button opens the enumeration editor, which is used to add or remove elements.

.. image:: _static/enumeration.gif

It is also possible to set the selected element using the value column in a model's list/tree-table view. The example below is in a DOME model.

.. image:: _static/enumerationList.gif

.. toctree::
   :titlesonly:

   editing-enumeration.rst
   methods-and-functions/index
