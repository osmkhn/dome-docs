Matrix Methods and Operators
=============================

Matrix methods and operators are used in Python expressions that manipulate parameters in relations. A brief summary is provided on this page and more detailed examples/explanations are available in the subsection links, or through the more details links. 
A Matrix parameter is considered true if it has a non-zero dimension, false if has no dimension or is null. For other data type methods and operators see the method and operators page.

Methods:
TODO: Tables

Operators:

Functions:

Type Conversions:

Comparitors:



.. toctree::
   :titlesonly:

   matrix-methods.rst
   matrix-operators.rst
   matrix-functions.rst
   matrix-conversions.rst
   matrix-comparitors.rst

