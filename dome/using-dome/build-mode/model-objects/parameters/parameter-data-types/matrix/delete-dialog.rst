Matrix Delete Dialog
=====================

The Matrix editor's delete button is used to open the delete dialog. The dialog can be used to delete multiple rows or columns from a specific location in the matrix.

.. image:: _static/matrixDelete.gif
