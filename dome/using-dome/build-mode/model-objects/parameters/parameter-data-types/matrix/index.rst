Matrix
========

The Matrix data type supports two-dimensional matrices of primitive integer or real numbers. Matrices support units and methods and operators that may be used in relations. Matrix elements are indexed from 0.
The matrix editor panel (shown below), appears in the parameter GUI definition tab and is used to set matrix dimensions, units, and the value of elements. The number of rows and elements may be edited directly in the row/columns fields, or using the add or delete buttons. If the fix size checkbox is selected it is not possible to change the dimensions of the matrix.
Element values may be typed directly into the matrix table or many elements may be selected and changed to a common value using the fill button. The type combination box in the upper right of the GUI controls whether matrix elements are real numbers or integers. 
The unit combination box is used to assign a unit to the matrix and the constraint button opens the matrix constraint editor.

.. image:: _static/matrix.gif

It is also possible to change the size and unit of a matrix in a model's value column. The example below is in a DOME model. When you click on the value cell an editor with row/column editing fields and a unit combination box become available.

.. image:: _static/matrixList.gif

.. toctree::
   :titlesonly:

   add-dialog.rst
   delete-dialog.rst
   fill-dialog.rst
   methods-and-operators/index
   matrix-constraints.rst
