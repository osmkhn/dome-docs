Boolean Methods
==============================

The Boolean data type supports supports a copyFrom method and a duplication method. The copyFrom method should be used (not the = operator) only if you require internal relation parameters to maintain types matching the relation interface parameters.
A summary of other Boolean data type methods, operators and functions are on the methods and operators page.

TODO: Add Table
