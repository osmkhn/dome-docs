Vector Fill Dialog
===================

The Vector editor's fill button is used to open the fill dialog. The dialog is used to specific the initial value of new elements that are added to the matrix. Additionally, if vector elements are selected before opening the fill dialog, the fill value can be used to change the value of multiple existing elements.

.. image:: _static/vectorFill.gif
