Data Type Chooser
==================

A parameter's data type may be changed using the change option in the parameter data type combination box, as shown below. This will open the data type chooser GUI.
This functionality allows you to add parameters quickly and decide on their data type later, or you can change your mind about a parameter's data type without having to remove the parameter. 

.. image:: _static/typeChange.gif


.. toctree::
   :titlesonly:

   gui.rst
