Parameter Constraints
======================

Data types that support constraints will have a constraint button in their editor panel. Both hard and soft constraints may be defined in the constraint editor. In run mode, violation of a hard constraint will stop model execution, while violation of a soft constraint will generate a warning in the message log.
Constraints are not yet available.