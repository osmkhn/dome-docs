Model Causality	View
==============================

The model causal view of relation parameters contains four filters: independents; intermediates; results; and indeterminates.
This view presents interface parameters from an overall or global model causality viewpoint, unlike the input/output/local indeterminate view, which limits its scope to the relation's internal causality.
This view is useful for seeing what input interface parameters or local indeterminate interface parameters are actually dependent parameters from a global model viewpoint.
The example relation below is also shown in the relation causal view example. In this case all of the relation input parameters are driven by external variables through mappings. Thus, even though they are inputs from the relation's viewpoint, they are intermediate variables from the overall model viewpoint.

.. image:: _static/modelCausality.gif
