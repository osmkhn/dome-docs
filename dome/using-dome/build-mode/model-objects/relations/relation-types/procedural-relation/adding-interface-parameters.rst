Adding Interface Parameters
==============================

Procedural relation interface parameters are added using the procedural relation add menu. 
When added, references to interface parameters will always initially appear in the input filter of the editor GUI. Once the causality of the relation has been defined, dependent interface parameters will move to the output filter. 
Interface parameters may also be added directly in the DOME Model list visualization.