Adding Mappins
==============================

The process for mapping procedural relation interface parameters to model parameters is outlined in the DOME Model mapping parameters section. Additionally, mappings may be defined using the procedural relation edit menu. 
Procedural relation mapping restrictions include:

 * Only compatible parameter data types may be mapped to each other. 
 * Only one model parameter may be mapped to an input interface parameter. However, a single output interface parameter may be mapped to multiple model parameters.