Procedural Relation Editor GUI
==============================

The procedural relation editor is inserted into the main relation GUI, as shown below. 
Within the interface parameters tab there are two filters, one for inputs and one for outputs. 
The internal causality tab is used to access the relation causality editor.
Procedural relations also require that you write the code it is execute in their body definition editor.
ToDo: add screen shot