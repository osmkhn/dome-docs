Procedural Relation Menus
=========================

The Procedural Relation context Menus are available when its GUI is open and in focus. The Edit Procedural Relation Menu and Add Menu are used to create the relation's interface.
ToDo: add screen shot


.. toctree::
   :titlesonly:

   add-menu.rst
   edit-menu.rst
