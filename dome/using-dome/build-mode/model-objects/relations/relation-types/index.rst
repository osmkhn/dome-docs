Relation Types
================

There are different types of relations specialized for different internal computational solving or with predefined operations or behaviors. This section details the purpose of different relation types and how to use their editor GUIs.
Types supporting user-defined relation bodies and custom behavior.

TODO: Tables



.. toctree::
   :titlesonly:

   procedural-relation/index
   equal-relation.rst
   iterator-relation.rst
   twin-relation.rst
