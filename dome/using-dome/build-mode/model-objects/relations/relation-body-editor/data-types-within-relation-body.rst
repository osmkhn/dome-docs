Data Types Within Relation Body
=================================

Relation interface parameters will always be of the type (e.g., Real or Integer) specified by the person who defined the relation. However, during execution of the Python relation body, the type of internal parameters may actually change to other types. This is because Python is not a typed language and the internal Parameter types are determined based on the data in an assignment.
Consider the following example. A procedural relation has the interface parameters...

TODO: Tables

and the Python body of the relation is:

c = a / b 
d = c

When the relation body executes, new internal real parameter objects are created in Python. The objects are assigned types and values as they were specified in the relation interface.
Then, the first line of the code is executed (c = a / b). The Integer "/" operator will divide their values and return a Real object with a value of 1.5. The = assigns the returned object to c.
Therefore, the internal c is a Real Parameter (not an Integer) with a value of 1.5.
Next, the second line will be executed (d = c). Thus, c is assigned to d. After the second line has executed, the internal d is a Real Parameter with a value of 1.5.
Finally, the output interface parameters are updated.

 * The interface parameter c is an Integer, so it is assigned the value 1 (from the Int() of the Real internal parameter c). 
 * The interface parameter d is a Real, so it is assigned the value 1.5 (from the Real internal parameter d). 

The example shows that the derived parameter types may vary from the units of the external interface parameters.

TODO: Tables

and the Python body of the relation is:
c.copyFrom(a / b) d.copyFrom(c)
Once again, when the first line of the code is executed (c.copyFrom(a /b)), the Integer "/" operator will divide their values and return a Real object with a value of 1.5. However, unlike the = assignment (which assigned the returned object to c) the copyFrom method converts the result to an Integer parameter and copies the value 1 into c. Thus, the internal parameter c is an Integer, matching the type of its corresponding relation interface parameter.
Thus, when the second line executes, c will be converted to a Real type and the resulting value will be copied into d, leading to the final output results being:

 * The interface parameter c is an Integer with a value of 1.
 * The interface parameter d is a Real with a value of 1.0.

TODO: Tables
