Python References
==============================

The Open Book Project makes available The Python Bibliotheca. Included is the Python version of Allen Downey's book How to Think Like a Computer Scientist in an interactive form where you can read the book and type Python code to try it out at the same time.
Standalone Python reference documentation is also online, but we find this style of documentation a bit difficult to use. 
A good book for learning the basics of Python quickly is The Quick Python Book by Daryl D. Harms and Kenneth McDonald.
A good detailed reference book for Python is the Python Essential Reference (2nd Edition) by David M. Beazley and Guido Van Rossum.