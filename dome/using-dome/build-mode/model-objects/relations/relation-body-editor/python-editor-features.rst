Python Editor Features
============================

The relation body editor provides color highlighting according to Python syntax. Additionally, all DOME interface parameters names are highlighted. The relation editor does not do automatic indentation, so the user must insert tabs according to Python syntax. 
Other editing features are listed in the table below.

TODO: Tables