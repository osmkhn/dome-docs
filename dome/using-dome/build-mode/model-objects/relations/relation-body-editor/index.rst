Relation body Editor
=====================

The relation body editor is the bottom panel in the relation GUI. The example below shows the body definition in a procedural relation. The body editor is present only in relations that support a custom body definition. Specifics are in the documentation for each relation type. 
The body editor is used to define mathematical or logical relationships between relation parameters using a Java implementation of the Python programming language. A custom Python editor provides a number of convenient features for defining relations. 
All relation types requiring a user-defined relation body also require their internal causality to be defined. Care must be taken to ensure that this causality definition is consistent with the code in the relation body. Additionally, it is important to have a basic understanding of how types and units are treated within a relation.
ToDo: add screen shot


.. toctree::
   :titlesonly:

   data-types-within-relation-body.rst
   python-editor-features.rst
   python-references.rst
   units-within-relation-body.rst
