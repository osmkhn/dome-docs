Relation Causality Tab
=======================

The Causality tab of a relation GUI is shown below. This panel shows how relation parameters depend upon each other. The edit button is used to define causality in relations that support custom body definitions. The edit button is disabled in relations that do not support custom definitions. 
A matrix visualization shows what-parameters-drive-what within the relationship. In the example below assume that the relation has interface parameters a, b, c, d, e. Assume the relation body definition equations are:

c = a + b 
e = c + d

Only interface parameters that are relation output or indeterminate have a row in the visualization matrix. All interface parameters with outputs depending upon them are associated with a column. Thus, for example, one can read that c depends upon a and b by reading across the row labeled c, and e depends upon c and d by reading across the row labeled e.

.. image:: _static/causalityTab.gif

Some relation types have a predefined causal structure, while other types require you to specify dependencies between interface parameters. Specific requirements are in the documentation of individual relation types. When working with relations that require you to provide a causality definition, the edit causality information button, as appears above, is available to provide access to the causality editor GUI.




.. toctree::
   :titlesonly:

   causality-editor-gui.rst
