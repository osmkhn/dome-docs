Relations GUI
===============

A procedural relation GUI is shown below with its definition tab selected. There is a generic relation GUI into which editors specific to different types of relations are inserted. When the definition tab or the relation GUI is selected (in build mode) the relation editor is available.
Relation GUIs are opened in a standalone window from a model's list view. 

 * The name field is used to edit the relation's name. Text field editing commands are available.
 * The main panel of the definition tab has three main components: the relation interface parameter editor; the relation causality editor; and the relation body editor. Which of these editors is available will vary with the relation type. Within this area there will be a button that opens a constraint editor for relations that support constraints. 
 * The documentation tab is used to access the relation's documentation editor.

.. image:: _static/proceduralRelation.gif