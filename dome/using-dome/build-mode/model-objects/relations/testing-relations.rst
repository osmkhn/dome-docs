Testing Relations
==============================

It is possible to test execute relations on their own without executing the entire models or going through the full deployment process. In order to test execute a relation and bring all parameters into a consistent state:

 * Select the relation to be tested in the model
 * Choose the Test Relation command in the model menu

The relation will then execute and make input/outputs consistent. Status information will be directed to the model's message log.
ToDo: add screen shot of the test option in the menu bar for A DOME model. Must mention that this is specifically for a DOME model as this is a more generic section.