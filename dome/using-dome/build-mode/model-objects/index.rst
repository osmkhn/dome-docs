Model Objects
==============

This section describes model objects that can be added to models, what they are for and how to work with their GUIs. 
 
 
 * Parameters represent different kinds of data.
 * Relations define how different parameters affect each other.
 * Context are used to organize objects in a model.
 * Filters automatically generate predefined object organizations.
 * Visualizations graphically display data from parameters in forms such as charts or graphs. 

Information about what objects are available within different types of models and instructions on how to add them into models is in the models section.


.. toctree::
   :titlesonly:

   parameters/index
   relations/index
   context/index
   filters/index
   visualizations.rst
   object-methods-and-operators.rst
   examples.rst
