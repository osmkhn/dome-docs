Simultaneously Add and Map Parameters
=============================================

You may map an existing parameter to a newly created parameter in the same step using the add and map option in the add menu. 

 * Use the edit menu to copy a parameter of interest into the clipboard. In the figure below the parameter Ef was copied.
 * Apply the add and map->last selection command in the add menu and a new parameter will be created and mapped to the original selection. In the figure below, a procedural relation was selected so that a new relation parameter was created (Ef1) and mapped to the parameter Ef.

.. image:: _static/addAndMap.gif
