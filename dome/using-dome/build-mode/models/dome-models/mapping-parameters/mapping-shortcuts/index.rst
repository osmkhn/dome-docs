Mapping Shortcuts
===================

There are convenient shortcuts for common mapping cases available in the add menu.

i) Map: mapping an existing model parameter(s) to an existing relation interface parameter(s) or existing model parameter(s). 

ii) Add and Map: mapping an existing model parameter to a relation and creating a corresponding relation interface parameter in the same step.

.. image:: _static/mappingShortcuts.gif


.. toctree::
   :titlesonly:

   simultaneously-map-and-add-relation.rst
   mapping-existing-parameters.rst


