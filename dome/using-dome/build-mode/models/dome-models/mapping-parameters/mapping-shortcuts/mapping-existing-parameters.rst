Mapping Existing Parameters
==============================

You may map an existing parameter(s) to another existing parameter(s) using the map option in the add menu. 

 * Use the edit menu to copy a parameter of interest into the clipboard. In the figure below the parameter Ef was copied.
 * Select the icon of parameter that you want to map to, and then apply the map->last selection command in the add menu. In the figure below the relation parameter RealParameter was highlighted and then Map->last selection was chosen. Note that Ef now appears in the mapping column for RealParameter.

.. image:: _static/map.gif
