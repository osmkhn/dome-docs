Adding the Map
===============

Once the model, model parameter, and relation interface parameter are selected, you can add a map by using one of the map commands in the edit mapping menu. It is often convenient to keep the clipboard viewer open so the list of model parameters available for mapping is always visible.
When a mapping is made, the mapped model parameter will be appended to the mapping list and the mappings column of the model. In the example below the parameter Ef has been mapped to x.

.. image:: _static/mappingAddMap.gif
