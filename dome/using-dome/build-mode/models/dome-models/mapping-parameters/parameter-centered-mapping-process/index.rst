Model Centered Mapping Process
===================================

In a model centered mapping process, the mapping tool is used to first pick model parameters that you want to work with and then map them to relation parameters. The process involves the following steps.

 * Selecting the model
 * Selecting a model parameter
 * Selecting a relation parameter
 * Adding the map

.. image:: _static/modelCentricMapping.gif


.. toctree::
   :titlesonly:

   selecting-the-model.rst
   selecting-a-model-parameter.rst
   selecting-a-relation-parameter.rst
   adding-the-map.rst


