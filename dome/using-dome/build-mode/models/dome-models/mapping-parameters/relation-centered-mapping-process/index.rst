Relation Centered Mapping Process
===================================

In a relation centered mapping process, the mapping tool is used to first pick the relation that you want to work with and then map its interface parameters to model parameters. The process involves the following steps.

 * Selecting a relation
 * Selecting a parameter in the relation
 * Selecting a model parameter
 * Adding the map

.. image:: _static/mappingRelationCentric.gif


.. toctree::
   :titlesonly:

   selecting-a-relation.rst
   selecting-a-relation-parameter.rst
   selecting-a-model-parameter.rst
   adding-a-map.rst


