Selecting Model Parameters
==============================

In order to select another parameter for mapping to the relation parameter, use the copy command in the edit menu. In this case the model parameter Ef is being selected.


.. image:: _static/mappingSelModParameter.gif

