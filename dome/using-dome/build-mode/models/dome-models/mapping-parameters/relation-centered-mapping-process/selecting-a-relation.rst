Selecting a Relation
=======================


The model/relation/interface combination box in the mapping tool GUI includes a list of all relations in the model. Any relation can be selected from the list so that selected interface parameters may be mapped to selected model parameters. In this case the relation ProceduralRelation is being selected.

.. image:: _static/mappingSelRelation.gif
