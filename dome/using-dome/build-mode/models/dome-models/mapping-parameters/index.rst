Mapping Parameters
====================

Mappings can be added and edited using the mapping tool, which can be opened from the tools menu. Using the mapping tool, relations may be mapped to model parameters in either a model centric or relation centric process. 
In most instances it will be most convenient to add new mappings using the mapping shortcuts available in the add menu. 
What are mappings for? If model parameters are to change as the result of a relationship executing, or if changes in model parameter data are to cause a relation to execute, model parameters must be mapped to a relation's parameters. When parameters are mapped they are, in effect, connected and will always reflect the same value. Mappings are commonly used to connect parameters used shared by different relations. 
Only parameters with compatible data types may be mapped to each other.

.. image:: _static/mappingParameters.gif


.. toctree::
   :titlesonly:

   relation-centered-mapping-process/index
   parameter-centered-mapping-process/index
   mapping-shortcuts/index




