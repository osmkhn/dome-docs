DOME Models
===========

DOME Models are fully implemented using only DOME parameters and relationships. They do not rely on any external programs to perform embedded computation. At times it may be convenient to implement simple models directly in a DOME model. DOME Models currently support sets of algebraic relationships without loops. DOME models support all types of DOME model objects.
To work on a new DOME model:
Put application in build mode.
Select New->Dome Model the build menu, or if you already have a DOME model open in build mode you can select Dome model -> New in the DOME Model menu bar.

.. image:: _static/openDomeModel.gif

.. toctree::
   :titlesonly:

   gui/index
   adding-model-parameters.rst
   adding-relations.rst   
   mapping-parameters/index
   adding-context.rst
   adding-filters.rst
   adding-interfaces.rst
   testing-dome-models.rst
   examples.rst
