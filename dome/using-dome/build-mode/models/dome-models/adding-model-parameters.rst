Adding Model Parameters
=========================

Model parameters are used to create persistent data in a model. Parameter data types available in DOME models are listed in the DOME model add menu section. All parameter data types are detailed in the building parameters section.
To add parameters to a DOME model...

 * Make sure the model's add menu is available by selecting the list visualization build view in the definition tab. 
 * Select Add->desiredParameterType from the add menu.

References to new parameters are be inserted into the model list visualization. The location of the reference is determined by what is selected when the parameter is added. Once added, parameters can be defined as a constant and many data types also support the definition of constraints.
References to parameters may be removed, or parameters may be deleted, from the model using the DOME model edit menu.

.. image:: _static/addParameter.gif