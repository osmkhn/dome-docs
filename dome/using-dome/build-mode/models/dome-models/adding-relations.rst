Adding Relations
===================

Relations are used to define interactions between parameters. Types of relations available in DOME models are listed in DOME model add menu section. All relation types are detailed in the building relations section. 
To add relations to a DOME model:

 * Make sure the model's add menu is available by selecting the list visualization build view in the definition tab. 
 * Select Add->desiredRelationType from the add menu. 

References to new relations will be inserted into the model list visualization. The location of the reference is determined by what is selected when the relation is added.
References to relations may be removed, or relations may be deleted, from the model using the DOME model edit menu.

.. image:: _static/addRelation.gif