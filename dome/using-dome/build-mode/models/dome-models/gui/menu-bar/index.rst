DOME Model Menu Bar
======================

The DOME model menu bar is available whenever a DOME model GUI is in focus and the definition tab is selected. This provides access to Dome model, edit, add, and tool functions. Depending on the standard view in use, all menu options may not be available.

.. image:: _static/domeMenuBar.gif

When the documentation tab is selected, the documentation context menus are available.


.. toctree::
   :titlesonly:

   dome-model-menu.rst
   edit-menu.rst
   add-menu.rst
   tool-menu.rst

