DOME Model Tool Menu
======================


The DOME model Tool Menu is available whenever a DOME model is in focus and the definition tab is selected. The Tool Menu provides access to the Mapping Tool and the Interfaces Tool, which open in separate windows.

.. image:: _static/toolsMenu.gif


