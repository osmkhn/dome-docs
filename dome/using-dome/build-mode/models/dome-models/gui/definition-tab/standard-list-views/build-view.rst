DOME Model List: Build View 
============================


Build View is the main view for defining and editing DOME models. All editing, adding, and tool functions are available. The different columns of the view are described in the standard list view page.
Within this view you have complete flexibility to edit the model and structure it using context as desired. It is also possible to change viewing scope to different context within the view.

.. image:: _static/buildView.gif

