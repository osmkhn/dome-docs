DOME Model List: Causal View 
============================

The Causal View is a standard list view that includes a number of filters to organize parameters according to the overall causal structure of the model: independents filter, indeterminates filter, intermediates filter, and results filter. This view, combined with the graph and DSM visualizations, are useful to develop and understanding of the model structure.

Since the contents of this view are generated automatically, one cannot add objects to the model in this view. The add menu is not available. However, copy and delete in the edit menu, along with mapping operations, are permitted.

.. image:: _static/modelCausalView.gif


