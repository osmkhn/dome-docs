DOME Model List: Object Type View
====================================

The Object Type View is a standard list view that includes filters that organize objects according to their type: parameter filter, relationship filter, and context filter, etc. This is a useful view to see only unique objects in a model without any multiple references (as are allowed in build view).

Since the contents of this view are generated automatically, one cannot add objects to the model in this view. There is no add menu associated with the view. However, copy and delete in the edit menu, along with mapping operations, are permitted.


.. image:: _static/objectListView.gif
