Documentation Tab
===================

The DOME Model documentation tab contains the documentation editor and invokes the documentation editor context menus. The model can not be edited when the documentation tab is selected.

.. image:: _static/documentationTab.gif
