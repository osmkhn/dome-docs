XML Visualiziation
====================

All models support an XML visualization that can be selected using the visualization combination box. This view displays the model's XML definition. For comparison, the XML below is from the same model shown in list view in the main model visualization page. 
This view currently does not support editing, but support for text editing is envisioned (but not a high priority). If you have made changes to the model since it was last saved, the refresh button should be used to get an updated version of the XML.

.. image:: _static/visualizationXML.gif
