Model Visualizations
=====================

The visualization combination box in all model GUIs is used to display model contents using different types of visualizations. 

 * the list (or tree table) visualization has a slightly different structure depending on the model type, so they are described in the GUI/definition tab section of the documentation for each model type. This is the visualization used for editing models.
 * the graph view can be used gain insight into the causal structure of models.
 * the DSM (Design Structure Matrix) view can be used to easily view what depends on what within a model
 * the XML view can be used to inspect the XML code that defines the model. 

.. image:: _static/visualizationCombo.gif


.. toctree::
   :titlesonly:

   graph.rst
   design-structure-matrix.rst
   xml.rst
