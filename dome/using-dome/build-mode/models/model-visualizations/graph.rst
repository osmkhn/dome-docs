Graph Visualization
======================

All models support a graph visualization that can be selected using the visualization combination box. This view is useful for exploring the causal flow between parameters. For comparison, the graph below is from the same model shown in list view in the main model visualization page. All objects are shown with both their icon and their name. Instructions for manipulating the graph are available at http://www.touchgraph.com/. Click on the image for the dynamic graph layout.
Editing is not supported in this view. Also, there is a problem with right mouse controls for Mac OSX that has not been addressed. Therefore, right mouse options are not available on Macs.

.. image:: _static/visualizationGraph.gif
