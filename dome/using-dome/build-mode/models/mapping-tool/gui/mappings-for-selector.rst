Mappings for Selector
==============================

The mappings for combination box provides a list of all parameters for the selected model/relation/interface. When a parameter is selected from this list, parameters mapped to it will appear in the mappings list. In this case the model parameter Ef is mapped to relation parameters IEf, tEf, and fEf.

.. image:: _static/mappingSelector.gif
