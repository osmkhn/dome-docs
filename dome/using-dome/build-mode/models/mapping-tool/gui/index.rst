Mapping Tool GUI
=================

The mapping tool GUI contains three elements.

 * the model/relation/interface combination box (to select a relation, model, or interface to be mapped)
 * the mappings for combination box (to select a parameter within the chosen model, relation, or interface for which you want to see mappings) 
 * the mapping list area (which lists all parameters mapped to the selected parameter).

Mappings are committed as soon as they are made. If you want to delete a mapping use the delete command in the edit mapping menu.

.. image:: _static/mappingGUI.gif


.. toctree::
   :titlesonly:

   model-relation-interface-selector.rst
   mappings-for-selector.rst
   mapping-list-area.rst
