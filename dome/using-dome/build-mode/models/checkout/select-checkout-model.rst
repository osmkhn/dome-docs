Model Checkout: Select Model Option 
=====================================


To checkout a model, select the Checkout model option in the build menu, as shown below. This will allow you to checkout any type of model.

.. image:: _static/checkoutModel.gif
