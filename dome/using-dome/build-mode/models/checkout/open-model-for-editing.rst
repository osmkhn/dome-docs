Model Checkout: Open File
==============================

Once all of the necessary files have been downloaded, you will be asked if you want to open the file. You may open the file now, or open it later using the standard open option in the build menu.

.. image:: _static/checkoutOpen.gif
