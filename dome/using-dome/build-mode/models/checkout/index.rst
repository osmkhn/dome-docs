Model Checkout
===============

Model checkout is used to retrieve any previously deployed model from a server, provided you have appropriate model editing privileges.
In order to check a model out for editing...

 * select the checkout model option using the build menu
 * login to the server from which you want to check a model out
 * select the desired model on the server
 * specify location where the checked out files should be placed in your own file space 
 * open the model file for editing 

Once you have edited the model, you can check the changes back into the server using the redeploy option in deploy mode.


.. toctree::
   :titlesonly:

   select-checkout-model.rst
   login-to-server.rst
   select-model-to-checkout.rst
   specify-download-location.rst
   open-model-for-editing.rst