Model Checkout: Select Model 
==============================

Once you have logged into a server, you may navigate the server file space using a browser. Locate and select the model that you wish to checkout. Only models for which you have editing permissions will be shown in the browser. 

.. image:: _static/checkoutSelectModel.gif
