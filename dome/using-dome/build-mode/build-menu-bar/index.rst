Build Menu Bar
==============


The Build Menu Bar is available at all times while in build mode. The build application-level menus (build, windows, help) are in plain (non-bold) typeface.

.. image:: _static/build-menu-bar.png

.. toctree::
   :titlesonly:

   build-menu.rst
   window-menu.rst
   help-menu.rst
