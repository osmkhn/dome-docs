Window Menu
===========
The build windows menu tracks all build mode windows that are open. It is also used to hide/show the build mode clipboard.

.. image:: _static/window-menu.png