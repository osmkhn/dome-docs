Mapping to Model Parameters
==============================

Interface parameters must be mapped to parameters in the underlying model if changes in interface parameters values are to affect the model. Parameters may be added to an interface and mapped to the model in a single operation. 
However, if interface parameters are added before corresponding parameters are defined in the model, mappings may be added as follows.

 * Copy the desired model parameter into the clipboard using the model's edit menu.
 * Select the definition tab of the interface editor GUI.
 * Make sure that either the build view or interface causality view is selected.
 * Select the interface parameter that is to be mapped to the copied model parameter.
 * Select Map->last selection or Map->from clipboard... from the edit interface menu. 

Additionally, one may map or edit interface parameter mappings using the mapping tool.
ToDO: add screen shot of the map option.