Interface Model View
==============================

The model view is a special view that is used to makes parts of the underlying model visible in the interface. For example, one might want to expose relationships in the model so users of the interface can better understand the algorithms used by the model.
The model view can only reflect objects within the interface's model. Thus it is only possible to add and map model objects, with the exception of context. Context can be added directly to structure the view's contents.
Objects in the model view are view-only, meaning that subscribers to the interface can see the objects at runtime but not change their values, and they cannot use the objects in build mode.
ToDo: Add screen shot
 