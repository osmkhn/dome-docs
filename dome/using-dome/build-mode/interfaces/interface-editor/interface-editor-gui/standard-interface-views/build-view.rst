Interface Build View
=============================

Build View is a standard interface view that may be used to add parameters to the interface using the edit interface menu bar (the interface causality view may also be used for this purpose). When the interface is deployed, parameters in the build view will be available to remote users for subscription. 
Within the build view, context may be added to organize interface parameters. At present it is not possible to add relations to an interface.
ToDo: add screen shot 
 