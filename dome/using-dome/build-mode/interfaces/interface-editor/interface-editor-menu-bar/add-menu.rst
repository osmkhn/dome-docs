Interface Editor Add Menu
============================


The interface editor Add menu is available when the definition tab of an interface GUI is selected.
When in build view, parameters may be added and context can be used to organize the view. In interface causality view only parameters may be added. The system causality view and model view do not support the addition of interface objects that are not mapped to model objects.  
ToDo: Screen shot of the interface Editor add menu 
 
