Building Interface After the Model
=============================================

If the model for which the interface is being created is already complete, interface parameters can be added and mapped to the model in one step.
To add and map interface parameters in one step

 * Copy the desired model parameter into the clipboard using the model's edit menu.
 * Select the definition tab of the interface editor GUI.
 * Make sure that either the build view or interface causality view is selected.
 * Select Add and Map->last selection or Add and Map->from clipboard... from the edit interface menu. 

ToDo: screen shot of interface with add and map menu expanded.