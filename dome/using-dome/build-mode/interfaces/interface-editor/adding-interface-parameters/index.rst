Adding Interface Parameters
============================

Interface parameters can be viewed as proxies linked to parameters in the model underlying the interface. When deployed, users are able to access, change, and write Dome Model relationships using interface parameters. The actual model is not exposed to users.
All data types available within Dome Models are available in an interface (details of the different data types are in the building parameters section).
There are two ways one may approach building an interface. You can build the interface first and then map it to the model, or you can start with the model and create mapped interface parameters in one step.
Interface parameters may be deleted from the interface using the interface edit menu.
ToDo: Include green interface parameter icon.


.. toctree::
   :titlesonly:

   building-interface-after-the-model.rst
   building-interface-before-the-model.rst
