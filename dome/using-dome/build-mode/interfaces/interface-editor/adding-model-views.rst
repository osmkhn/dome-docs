Adding Model View Elements
==============================

The Interface model view is provided so that a 'hole' can be made through an interface, allowing users to observe selected objects in the model when it is deployed.
To add model objects to the interface model view...

 * Copy the desired model objects into the clipboard using the model's edit menu.
 * Select the definition tab of the interface editor GUI.
 * Make sure that either the interface model view is selected.
 * Select Add and Map->last selection or Add and Map->from clipboard... from the edit interface menu. 

The model view may also be structured by adding context. If you want to place model view object in a context, the context must be added before the model objects are added and mapped.
ToDo: add screenshot of the model view and add and map.