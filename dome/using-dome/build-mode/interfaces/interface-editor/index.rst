Interface Editor
==================

The editor for a model interface is opened from the interface tool/manager. The interface editor allows you to add parameters to an interface and map them back to parameters in the model.


.. toctree::
   :titlesonly:

   adding-interface-parameters/index
   interface-editor-gui/index
   interface-editor-menu-bar/index
   adding-context.rst
   adding-custom-gui.rst
   adding-model-views.rst
   importing-parameter-documentation.rst
   mapping-to-model-parameters.rst