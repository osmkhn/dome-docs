Building Interfaces
=====================

All models are executed through interfaces. Interfaces define what parameters and views of the model will be accessible by remote users. Models support multiple interfaces, so you can create different interfaces customized for different users of the same model.
A default interface is automatically built for every model so that you can test or deploy a model without the step of explicitly defining an interface. 
All interfaces associated with a model are viewed, created and deleted using the interface manager, which is accessed through the model tools menu. The contents of interfaces are defined and mapped to model parameters using the interface editor.


.. toctree::
   :titlesonly:

   default-interface.rst
   interface-examples.rst
   interface-versioning.rst
   testing-interfaces.rst
   interface-manager/index
   interface-editor/index
