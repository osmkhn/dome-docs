Default Interface
==============================

A default interface named "Default Interface" is automatically generated for every model. This allows you to test the model during the building process without having to take the time to manually define an interface before testing. It also allows you to deploy a model without defining an interface yourself, provided you are satisfied with the items that are automatically exposed in the default interface.
The default interface is not user editable. However, in the interface manager, you can duplicate the default interface and use the duplicate as a starting point for defining other interfaces.
The standard interface views in the default interface are populated as follows.

 * The build view contains a flat list of all independent and result parameters in the model. The view only context exposes the entire model for browsing by model users.
 * The interface causality view displays all independent model parameters in the input filter, and all model result parameters in the output filter.
 * The system causality view will be the same as the interface causality view unless the interface has already been connected to other remote models.
 * The model view reveals all objects in the underlying model. This will allow you to view the state of all model variables when the model is tested.