General Build GUI Behavior
===========================

There are number of general behaviors associated with build mode GUIs. 
This section discusses list view or tree view editing behavior, the yellow background stale color, text field edit commands,and tree view insertion.


.. toctree::
   :titlesonly:


   list-tree-views.rst
   stale-color.rst
   text-field-editing.rst
   tree-view-insertion.rst