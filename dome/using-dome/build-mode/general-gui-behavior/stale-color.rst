Stale Color
===============

Build mode text fields have a stale background color (yellow) to indicate when changes have not been committed. When a change is not committed, editing inputs only reside in the GUI window. However, the corresponding data in the object has not been altered. When changes are committed the information displayed in the GUI will be consistent with the object data and the background color of editing fields is white.
Mechanisms to commit changes or cancel changes in editing fields are consistent with the behavior of list or tree views. 
An example of the stale background color (using the real number parameter GUI) is below.

.. image:: _static/buildStale.gif
