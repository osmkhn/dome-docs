Documentation Editor
=====================

All DOME objects that support documentation will have a documentation tab in their GUI, The documentation tab contains the documentation editor, as shown below (within a DOME model GUI).
The URL field at the bottom of the editor may be used to point to an external documentation page on a web server. The open button can be used to test the URL.
Documentation may also be entered directly into the comments field. The build documentation menu allows for simple formatting and editing of comments.

.. image:: _static/documentation.gif

.. toctree::
   :titlesonly:

   documentation-menu.rst