Window Management
=======================

Keeping track of windows in multiple-window applications can be a challenge and DOME3 is no exception. Therefore, a number of mechanisms are provided to help avoid window bloat.
* Window menu support for locating open windows (in the DOME3 menu bar).
* Windows are color coded by model (colored square icons are in the left corner of the window title bar), making it easy to recognize all windows that belong to objects in the same model (not available on Mac OS X).
when a model is minimized, all other object GUIs associated with the model are also minimized.
* A minimized window can be opened by clicking on its icon.
* when you move from one application mode to another, all open windows associated with the mode are hidden. They will reappear when you return back to the mode.