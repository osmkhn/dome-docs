Step 2: Deploy the Model 
===========================

In this step you will deploy the updated Rectangular solid with hole and custom GUI model to a server where it can be executed.
During the deployment process the files needed for the custom interface GUI are transferred to the server, allowing remote users to use the custom interface GUI.
Deploying models is covered in many different tutorials, so only key steps are covered.
 
.. image:: _static/addingCustomRect/step2/Picture-1.gif

Figure 1: Using the deploy wizard to log into your server.

In deploy mode use the Deploy-->Model menu to open the deploy wizard. Log into your account on the server that you have been using for the tutorials, as in figure 1. joe smith is the tutorial placeholder for your user name.

.. image:: _static/addingCustomRect/step2/Picture-2.gif  

Figure 2: Select the your model.

Proceed to the Select model page and then browse to select the model that you modified and saved in step 1. This is shown in figure 2.

.. image:: _static/addingCustomRect/step2/Picture-3.gif  

Figure 3: Adding a description for the model.

Add the description as shown in figure 3. This will appear beside the model name in the run browser.

.. image:: _static/addingCustomRect/step2/Picture-4.gif  

Figure 4: Selecting the location of the model on the server.

Proceed to the Select location on server panel. Select the tutorial models folder inside your Public directory, as has been done in figure 4. 

.. image:: _static/addingCustomRect/step2/Picture-5.gif  

Figure 5: Selecting interfaces to deploy.

Proceed to the Select interfaces to deploy panel. Select the volume and cost interface for deployment. If you wish, you may also make the default interface available.
Complete the remaining deployment steps and deploy the model to the server.