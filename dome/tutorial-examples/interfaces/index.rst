Interfaces
===========================
This set of tutorials focuses on different aspects of model and project interfaces. All Interface tutorials assume that you have covered the running published models tutorials. Additional prerequisites may be required in the individual tutorials.

The interface tutorials available are: 

1. Using a Custom GUI for the rectangular solid model: You use a custom interface GUI in run mode.

2. Adding a Custom GUI to the rectangular solid model: You will add an existing Custom GUI to a model interface.

3. Writing a custom GUI for the rectangular solid model: You will write a custom JAVA swing GUI for a model interface.

.. toctree::
   :titlesonly:

   using-custom-gui.rst
   adding-custom-gui/index
   writing-custom-gui.rst
