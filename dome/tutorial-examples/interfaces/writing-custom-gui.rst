Writing Custom GUI for Rectangular Solid 
=========================================

This tutorial provides a very brief overview of how to write a custom interface GUI. It is useful only if you plan on making your own custom GUIs.
It is assumed that you know how to work with Swing and are comfortable with Java development.
Source code is for the custom GUI used in the Using custom GUI for the rectangular solid model and the Adding a custom GUI to the rectangular solid model tutorials. 
Some key points and code excerpts are discussed below.

.. image:: _static/writingCustomRect/GUIpicture.gif

The complete source code for the custom GUI shown above is in dome3/tutorialModels/interface/rectSolidCustomGUI/VolumeAndCostGUI.java

.. image:: _static/writingCustomRect/Picture-1.gif

Figure 1: DOME classes that need to be imported.

Figure 1 shows the DOME classes that are needed for the custom GUI. You will need to include dome.jar as a library. 
The CustomGui and ModelInterfaceBase classes are essential. The Templates class in not required--it is a convenience class that is used throughout DOME for creating Swing components. The Templates class was used in this custom GUI.
JavaDoc is available for the CustomGui and Templates classes.

.. image:: _static/writingCustomRect/Picture-2.gif
 
Figure 2: Constructors for the custom GUI.

You will need to write two constructors for the class. The top constructor in figure 2 is the one that will be used by the DOME interface. The second constructor is a standard constructor that can be used to run the GUI for standalone testing. 
The constructor used by the DOME interface calls the standard constructor and then the additional setInterface method.

.. image:: _static/writingCustomRect/Picture-3.gif

Figure 3: Set interface method for the custom GUI.

The set interface method connects the value of the DOME interface parameters to Swing components in your custom GUI. For example, 'diameter' is the name a parameter in the DOME interface and holeDiameter is a JTextField in the custom GUI.
There are a number of static methods in the CustomGUI class for connecting parameters containing various DOME data types to different Swing components.

.. image:: _static/writingCustomRect/Picture-4.gif

Figure 4: Using the Templates class.

Figure 4 shows an example of using the DOME Templates class to create a DOME formatted label. A large number of Swing components can be created using this convenience class. 
 You are not required to use this convenience class when making your own custom GUIs.

.. image:: _static/writingCustomRect/Picture-5.gif

Figure 5: main method needed to test your GUI independent of DOME.

If you want to test your custom GUI as a standalone component, you will need to define a main method similar to the one in figure 5.