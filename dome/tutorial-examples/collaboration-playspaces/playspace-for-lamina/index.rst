Playspace for Lamina Design Team
=================================

This tutorial covers introductory aspects of building and deploying a playspace. Please check that you have covered the prerequisites for the tutorial as outlined on the main playspace tutorials page. 
You will create and run a playspace containing models and projects related to carbon/fiber composite lamina. You worked with these models in the running models as a guest and user tutorials.

There are three steps in the tutorial.

Step 1: You will open a new playspace using build mode and then select models to be accessed through it.

Step 2: You will use a deploy wizard to move the playspace into your user account and to set access permissions.

Step 3: You will run models through the newly created playspace and use a playspace chat tool to communicate with other users of the playspace.

.. toctree::
   :titlesonly:

   step1.rst
   step2.rst
   step3.rst

