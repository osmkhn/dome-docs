Step 1: Build Lamina Team Playspace 
====================================


In this step you will use build mode to start a new playspace. You will add models and projects to the playspace.
You will learn how to: open a new playspace; locate models for the playspace; and add selected content to the playspace. 

.. image:: _static/step1/Picture-1.gif

Figure 1: Opening a new playspace.

Begin by using the Build-->New playspace menu item to open a new playspace, as shown in figure 1.

.. image:: _static/step1/Picture-2.gif

Figure 2: The playspace build window.

The playspace window that will appear is shown in figure 2. Its functionality is very simple--it allows you to give the playspace a name and add/remove content.

.. image:: _static/step1/Picture-3.gif
 
Figure 3: Changing the playspace name.

Change the playspace name to 'Lamina design team', as shown in figure 3. This is the name that users will see while using the playspace in run mode.

.. image:: _static/step1/Picture-4.gif
 
Figure 4: Opening the add models window.

Now, you will add content to the playspace. 
Press the add button highlighted in figure 4 and the add models window will open. This window contains the familiar run browser.

.. image:: _static/step1/Picture-5.gif
 
Figure 5: Logging in to your account.

Press the login button in the browser and then log into the server that you have been using for the tutorials. In figure 5 there is a cached login for the tutorial 'placeholder user' joe smith.

.. image:: _static/step1/Picture-6.gif
 
Figure 6: Switch to the server file space.

You will need to change from you user account file space to the server (adminstrator group) file space, as shown in figure 6. This is where the tutorial models that will be used in the playspace are located.

.. image:: _static/step1/Picture-7.gif
 
Figure 7: Making models available within the playspace.

Expand the folders in the server file space so that your view matches figure 7. Select the Epoxy/Carbon lamina model and then press the put selection in playspace button. The model will appear in the playspace window as highlighted in the figure.
Repeat this process to add the Polymer curing model and the As cured lamina properties project to the playspace.
Note: The content of the model is not actually moved to the playspace. The playspace only contains a URL. Thus, when models are run through a playspace they are still executed on the server where they were originally deployed, and user access to interfaces is still determined by those defined by the original publishers of the model.

.. image:: _static/step1/Picture-8.gif
 
Figure 8: The playspace containing three models and a project.

Press the done button on the add models window. Your playspace will now look like figure 8.

.. image:: _static/step1/Picture-9.gif
 
Figure 9: Saving the playspace.

The final step is to save the playspace. 
Use the DOME Playspace-->Save menu item as shown in figure 9.

.. image:: _static/step1/Picture-10.gif
 
Figure 10: Naming the playspace file.

Navigate to where you want to save the playspace in your computer file system. Give the playspace file the name Lamina team, as shown in figure 10.

.. image:: _static/step1/Picture-11.gif
 
Figure 11: The completed and saved playspace.

Figure 11 shows the completed playspace. The path and name of the playspace file is in the dark text field at the bottom of the playspace window. 
The playspace is ready to be deployed. Use the Dome Playspace-->Close menu item to close the playspace build window bore moving on to the next steps.