Step 2: Deploy Lamina Playspace 
================================

In this step you will move the playspace file that you saved in step 1 to a server where it can be accessed by remote users.
You will learn how to: check playspace files into a server using a deploy mode wizard; and assign use permissions that will determine who can enter the collaborative playspace.

.. image:: _static/step2/Picture-1.gif

Figure 1: Opening the deploy playspace wizard.

Put the DOME client application in deploy mode and select the Deploy-->Playspace... menu item to start the deploy wizard, as in figure 1.

.. image:: _static/step2/Picture-2.gif
 
Figure 2: Completed login phase.

Log in to your user account on the server that you have been using for the tutorials. This step is completed in figure 2 for the placeholder user joe smith.
Press the next button to proceed with the deployment process.

.. image:: _static/step2/Picture-3.gif
 
Figure 3: Select the Lamina team playspace file.

Use the browse button to open the file chooser as shown in figure 3. Select the Lamina team file that you saved in step 1.

.. image:: _static/step2/Picture-4.gif
 
Figure 4: Adding a description.

If desired, add a description to appear with the playspace name in run mode. In figure 4 the description 'My first collaboration work area' has been entered.
Press the next button.

.. image:: _static/step2/Picture-5.gif
 
Figure 5: Locating the playspace on the server.

The next step it to define where the playspace is to be located on the server. Note that, as highlighted in figure 5, playspaces are located in the collaborative use portion of your file space.
Select the Public folder. Use the add folder button (also highlighted in the figure) to create a new folder. Give the folder the name 'tutorial playspaces'.

.. image:: _static/step2/Picture-6.gif
 
Figure 6: Selecting the playspace destination folder.

Select the tutorial playspaces folder as the destination for you playspace as in figure 6, and then press the next button.

.. image:: _static/step2/Picture-7.gif
 
Figure 7: Setting Edit permissions for a playspace.

Now, you will determine who can edit the contents of playspace. Give yourself editing permissions.
The playspace edit permission set shown in figure 7 is very similar to edit permissions for models and projects. The meaning of the different edit permissions are summarized below.
Set playspace editing permissions: provides permission to decide who can set the edit permissions. If you do not have this permission you will not be able to set any of the permissions in this list.
Delete playspace: provides permission to delete the deployed playspace from the server during server file space management.
Modify playspace: provides permission to check out the playspace from the server for editing, and to redeploy the edited playspace onto the server as a version update of the original.
Copy playspace: allows a user to check out the playspace, but not redeploy it as a version update to the original. The user can only deploy it as a different playspace. Since copy is a subset of modify, this is always available when modify permissions are available.
Set playspace use privileges: This is available only if you have modify or copy permissions. This gives you permission to decide which users can join the playspace in run mode.
Concepts related to checking out models/projects/playspaces for editing and redeloyment were covered in step 6 of the rectangular soild with hole tutorial.
Proceed to the next step. 

.. image:: _static/step2/Picture-8.gif
 
Figure 8: Adding users to the playspace.

Add guest as a user to the playspace as in figure 8. This means that guest logged into the server will be able to see and join the playspace. However, what they can see of the models inside the playspace is determined by the individual model use permissions.
A summary of the use permission list is below.
Change values: allows the user to run models in the playspace, provided that they have use permission for the specific model.
Save state versions: the current implementation does not yet have the functionality related to this permission.
Open as a separate playspace: the current implementation does not yet have the functionality related to this permission.

.. image:: _static/step2/Picture-9.gif
 
Figure 9: Users added to the playspace.

Figure 9 shows shows guest and yourself added as users/members of the collaborative playspace.
Proceed to the next step.


.. image:: _static/step2/Picture-10.gif
 
Figure 10: Summary of deployment options.

The final step provides you with a summary of your deployment options. Press the deploy button to move the playspace file to the server.

.. image:: _static/step2/Picture-11.gif
 
Figure 11: Confirmation of successful deployment.

When deployment is complete, a confirmation dialog will appear as shown in figure 11. 
Close the playspace wizard.