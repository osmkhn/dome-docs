Collaboration Playspaces
===========================

This section of the tutorial is intended to familiarize you with how to build and deploy playspaces.
These tutorials assume that you are familiar with materials covered in the running published models tutorials, and that you have access to a DOME server on which you have been provided with a user account with file space (so you can upload playspaces to the server).

If you do not have such an account, you should create and account for yourself on a local server that you can run on your computer. You can do this be completing the first step of the server management as an administrator tutorial now.

Tutorials Available:

1: Playspace for lamina design team. This tutorial covers building and deploying a playspace that contains models and projects described in the running published models tutorial.

.. toctree::
   :titlesonly:

   playspace-for-lamina/index
