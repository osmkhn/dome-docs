Excel Models
==============

This section of the tutorial is intended to familiarize you with how to build and deploy Excel-based plugin models. Please check that you have covered the prerequisites as outlined on the main plugin tutorial page. The tutorials also assume that you are familiar with Excel.

Tutorials Available: 

1: Excel model of rectangular solid with hole. This tutorial covers building a simple spreadsheet in Excel, wrapping it in a plugin model, creating interfaces for the model, deploying the model and setting use permissions, and running the model.

.. toctree::
   :titlesonly:

   rect-solid-with-hole/index
