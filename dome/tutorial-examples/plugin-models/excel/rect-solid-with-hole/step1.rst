Step 1: Building the spreadsheet 
==================================

In this step you will make the spreadsheet needed to compute the volume and cost of the rectangular solid with a hole.
The goal of this step is to prepare a spreadsheet so that it can be wrapped and deployed for use in DOME in the subsequent steps of the tutorial. 
If you do not want to build your own spreadsheet, you can find a prepared file in the DOME/tutorialModels/plugins/excel/rectSolid folder.

.. image:: _static/step1/Picture-1.gif

Figure 1: Excel spreadsheet for the rectangular solid with a hole.

The spreadsheet needed for the tutorial is shown in figure 1. 
If you build the spreadsheet yourself, it is recommended that you copy the spreadsheet exactly (including cell locations) so that your steps in later portions of the exercise will match the tutorial information.
The cell equations are as follows.

width: D2 = 2*B2

length: D3 = 3*B2

volume: D4 = D3*D2*B4-(B4*B2^2*PI()/4)

cost: D5 = D4*B5