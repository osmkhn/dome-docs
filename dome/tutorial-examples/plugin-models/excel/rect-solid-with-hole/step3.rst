Step 3: Building an Interface
================================

In this step you will create an interface for the model that you completed in step 2. Interfaces are the exposed views of a model made available to run-mode users.
The process of building interfaces is the same for DOME models, plugin models, and integration projects. Therefore, this step of the tutorial is a quick summary of the process.
 
.. image:: _static/step3/Picture-1.gif

Figure 1: Opening the interfaces tool.

Begin by opening the interfaces tool using the Tools-->Interfaces menu shown in figure 1.

.. image:: _static/step3/Picture-2.gif
 
Figure 2: Add a new interface.

Rather than using the default interface, we will create a new interface from scratch. Use the Interfaces-->New menu item as shown in figure 2.

.. image:: _static/step3/Picture-3.gif 

Figure 3: Renaming the new interface.

Give the new interface the name 'volume and cost interface' as in figure 3. Double click on the interface icon to open it in a new window.

.. image:: _static/step3/Picture-4.gif 

Figure 4: Add context to the interface.

Use the Add-->Context menu item to add two context to the interface. Name them as shown in figure 4.

.. image:: _static/step3/Picture-5.gif
 
Figure 5: Adding model parameters to the clipboard.

Switch back to the model window and select the two independent parameters hole diameter and block height. Copy them into the clipboard using the Edit-->copy menu item as shown in figure 5.

.. image:: _static/step3/Picture-6.gif
 
Figure 6: Adding the copied parameters to the interface.

Return to the open interface window, expand and select the free dimensions context, and then use the Add-->Add and Map-->Last selection menu item to add the parameters to the interface.

.. image:: _static/step3/Picture-7.gif
 
Figure 7: The completed interface.

Repeat the process of copying parameters in the model into the clipboard and then Add and Mapping them into the interface until the interface is complete, as shown in figure 7.

.. image:: _static/step3/Picture-8.gif 

Figure 8: Save the model.

Finally, use the Excel Model-->Save menu item to save your interface changes. The wrapper model is now ready for deployment.