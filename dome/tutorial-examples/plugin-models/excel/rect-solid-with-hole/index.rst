Excel Model of a Rectangular Solid with a Hole
================================================

.. image:: _static/blockOnly.gif

This tutorial covers several aspects of building Excel models. 
You will model the volume and cost of a rectangle with a hole through its center. The model will be functionally equivalent to the model you made in the first DOME model tutorial. This will allow you to compare the two implementations.

The tutorial is divided into the following sequential steps. 

Step 1. Making the spreadsheet: You will make a spreadsheet to represent the volume and cost of the rectangle with a hole.

Step 2. Wrapping the Excel model: You will open a new Excel plugin model, add parameters to the model and connect them to cells in the Excel spreadsheet, define the model's causality, and configure the model so that it can locate your spreadsheet.

Step 3. Building interfaces for the model: You will create an interface for the model that defines a view that will be used to execute the model in run mode. 

Step 4. Deploying the model: You will deploy the model into your user account and set access permissions on the deployed model.

Step 5. Running the model: You will execute the newly deployed Excel model and compare it with the functionally equivalent DOME model created in an earlier tutorial.

.. toctree::
   :titlesonly:

   step1.rst
   step2.rst
   step3.rst
   step4.rst
   step5.rst

