Plugin Models
===========================

This section of the tutorial is intended to familiarize you with how to build and deploy plugin models. 
All of the plugin tutorials assume that you are familiar with materials covered in the running models tutorials and that you have completed the first DOME model tutorial to the extent that you are comfortable will adding parameters to models, defining causality in relations, creating model interfaces, and deploying models. 
Building and deploying a plugin model is quite similar to building a DOME-native model, with the exception that relations cannot be added to a plugin model. You can think of a plugin model as a being similar to a single DOME relation, with the relation's computation being performed by a 3rd party software application.

Tutorials are available for: 

 * Excel 


.. toctree::
   :titlesonly:

   excel/index
