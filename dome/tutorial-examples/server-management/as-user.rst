Server Management As a User
=============================

This tutorial is intended to provide an overview of server administrative capabilities available to you as a user on a dome server. This includes managing your password and your server file space (if you have been provided with one).

Step 1: Changing your password.
It is assumed that you have a local server running on your computer and that you are running the DOME client application. It also assumes that you have a user account on the server. If you do not have a user account, see step 1 of the server management as an administrator tutorial.

.. image:: _static/asUser/1_serverMode.gif

Figure 1: Switching the DOME client to server mode.

Make sure that your DOME client is in server mode using the mode combination box as shown in figure 1.

.. image:: _static/asUser/2_login.gif
 
Figure 2: Opening the login window.

Use the login option under the Server menu to bring up a login window (figure 2). 

.. image:: _static/asUser/3_loginCache.gif
 
Figure 3: Logging in as joe smith.

If you just completed the server management as administrator tutorial or the DOME model, project, or playspaces tutorials you will probably have your user login cached, as illustrated in figure 3 (remember that joe smith is the tutorial placeholder for your user name). If your login is cached, select it in the recent logins combination box and then press the login button highlighted in the top of the window.
If your login is not cached, complete the login information in the bottom part of the window and press the lower login button. You do not need to specify the server location to log into a local server running on the same computer as your client application.

.. image:: _static/asUser/4_passwordOption.gif
 
Figure 4: Opening the change password window.

Select change password from the Options menu as in figure 4 to open the password change window. 

.. image:: _static/asUser/5_changePassword.gif
 
Figure 5: Entering the new password.

Figure 5 shows the change password window for the user joe smith. Type in your existing password (123 for joe smith) in the old password field and then type in your new password twice, once in each of the two new password fields. 
Be sure to use a password that you can remember! When you are done press the OK button.

Step 2: Managing server file space.
If you have permission to deploy models and playspaces onto a server, you will want to manage your file space. At present the file management tools are very primitive.

.. image:: _static/asUser/6_manageFiles.gif
 
Figure 6: Opening the manage file space window.

Select manage file space from the Options menu to open a file space window.

.. image:: _static/asUser/7_userFileSpace.gif
 
Figure 7: View of the server file space for joe smith.

The window will open displaying your file space for individual use. If you expand the folders as shown in figure 7, you should be able to recognize it as your space on the server. 
The editing controls are in the bottom right of the window. 
Models, projects and folders can be deleted by first selecting and then pressing the delete button. Folders can be deleted only if they are empty. 
The public and private folders cannot be deleted. Public means that any user with permission will be able to browse the folder in run mode to view content within (provided that permissions for the models/projects within allow the given user to see them). Private means that only you can browse items within the folder.
Only folders can be renamed. This is done by selecting the folder and pressing the rename button. If you want to rename a model, project, or playspace you will need to change the name in build mode and then redeploy.
The folder button in the bottom right is used to add new folders. New folders can be added to folders only. Select the folder you would like to put a new folder into and then press the folder button.

.. image:: _static/asUser/8_collaborativeSpace.gif
 
Figure 8: Switching to the collaborative use space.

If you have a file space for collaboration playspaces on the server, you will be able to use the view combination box to switch to this file space, as shown in figure 8.
This file space can be managed in the same manner as the individual use file space. 

.. image:: _static/asUser/9_groupSpace.gif
 
Figure 9: Switching to the groups file space.

If you are a member of any groups with file space on the server, you will be able to switch to the groups file space as shown in figure 9. Only groups that you are a member of will be shown.

.. image:: _static/asUser/10_logout.gif
 
Figure 10: Completing the tutorial by logging out.

Close the file management window using the X in the window title bar. Logout as shown in figure 10 to complete this tutorial.