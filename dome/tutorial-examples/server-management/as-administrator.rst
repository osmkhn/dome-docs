Server Management as an Administrator 
=======================================

This tutorial is intended to provide an overview of server administrative capabilities available to members of the administrators group. This includes: managing users and their passwords; groups; your own password; and the server file space. 
If you are completing this tutorial to proceed with the DOME models tutorial, you only need to do step 1 at this time.

Step 1: Creating users, passwords and adding users to groups.

It is assumed that you have a local server running on your computer and that you are running the DOME client application.

.. image:: _static/asAdmin/1_serverMode.gif
 
Figure 1: Switching the DOME client to server mode.

Make sure that your DOME client is in server mode using the mode combination box as shown in figure 1.

.. image:: _static/asAdmin/2_loginMenu.gif
 
Figure 2: Opening the login window.

Use the login option under the Server menu to bring up a login window (figure 2). 

.. image:: _static/asAdmin/3_loginDialog.gif
 
Figure 3: Entering login information.

In a default server installation, the user root is a member of the Administrators group. Enter the user name root in the user name field as shown in figure 3. The default password for the user root is cadlab. For security reasons you should change the root password (this is covered in step 3 of this tutorial).
Any server can be administered from a computer running the DOME client application in server mode. In this tutorial, it is assumed you will be logging into a server running on the same computer as your client. Therefore you do not need to enter a location or machine name in the server field. If your server is on another computer you will either need to provide the machine name or IP address.
Select the administrator radio button to login as an administrator. By default, 'login as' is always set to user.
Once your login window is completed as shown in figure 3, press the login button.

.. image:: _static/asAdmin/4_listUsers.gif
 
Figure 4: Listing the users on the server.

Under the Users menu select the List Users option as shown in figure 4. This will open the user list window.

.. image:: _static/asAdmin/5_userList.gif
 
Figure 5: The user list window.

The user window list will look like figure 5 if your server has the default installation configuration. This is a list of all users known to the server. 
The green circle to the left of each user name indicates that the user is active and they will be able to log into the server. If the circle is red, the user has been made inactive and will not be allowed to log into the server.

.. image:: _static/asAdmin/6_newUser.gif
 
Figure 6: Opening a new user dialog.

The next step will be to make a user account for yourself. From the Users menu, select the New option as shown in figure 6.

.. image:: _static/asAdmin/7_defineUser.gif
 
Figure 7: Completing the new user information.

Enter the user name you would like to have in the name field. In tutorials the user joe smith will be a placeholder for your chosen user name. 
You can enter a description for yourself if desired. The description is optional. 
A password is required and must be entered twice in the two lines adjacent to the password label. You will be prompted if the two entries do not match. Choose a password that you can remember. The password for joe smith is 123.
Check can save models so that, in addition to just logging in to the server, file space on the server will be allocated so that you can deploy models that you create onto the server. Similarly, check can save playspaces so that you will be able to deploy playspaces onto the server. 
Finally, press the add button and the new user will be added to the server database.

.. image:: _static/asAdmin/8_editUser.gif
 
Figure 8: Opening the edit user window.

Next, you will edit the the newly defined user. Click on joe smith in the Users List window and then select edit from the Users menu as show in figure 8.

.. image:: _static/asAdmin/9_groupList.gif
 
Figure 9: The edit user window and the view/edit groups window.

The edit user window in figure 9 allows you to change: the user's description; password; group membership; whether they can save models or playspaces on the server, and whether they are active or inactive. 
User names are not editable. You can either delete an old user or make them username inactive so they cannot log into the server. 
Click on the view/edit groups button and the view/edit groups window will open as shown on the right of figure 9.

.. image:: _static/asAdmin/10_selectGroup.gif
 
Figure 10: Adding the user to the tutorialGroup.

You will use the view/edit groups window to add joe smith to the tutorialGroup. This will allow joe smith to access models that have been provided on the server for several tutorials. Members of the tutorialGroup were given permission to use the tutorial models.
Click on the tutorialGroup so that it is selected as shown in figure 10. Then click on the left arrow to make joe smith a member of the group.

.. image:: _static/asAdmin/11_addedToGroup.gif
 
Figure 11: joe smith is a member of the tutorialGroup.

As shown in figure 11, joe smith will be a member of the tutorial group once you press the commit button. Commit the changes now.
To complete step 1 of this tutorial, click OK in the edit user window and the close button on the user list window. 
If you have been completing this tutorial to provide yourself with a user account for the DOME models tutorial, you can return to the DOME models tutorial now.

Step 2: Creating groups and adding users to groups.
You are currently logged in as a member of the administrators group, which gives you access to define users and groups. You will now go through a similar process to create a new group on the server and add users to the groups.

.. image:: _static/asAdmin/12_listGroups.gif
 
Figure 12: Opening the List Groups window.

Use the List Groups item in the groups menu to open the group listing window as shown in figure 12.

.. image:: _static/asAdmin/13_newGroup.gif
 
Figure 13: Opening the new group window.

If your server is in the default installation configuration, there will be only two groups. The other group available is the tutorialGroup of which joe smith is now a member. 
To add a new group, select new in the Groups menu as shown in figure 13.

.. image:: _static/asAdmin/14_defineGroup.gif
 
Figure 14: Competing information needed to make a new group window.

Complete the information as shown in figure 14 to define the new group. Remember that joe smith is a placeholder for the user name that you created in step 1 of this tutorial. Permit space for deployment into the joe smith friends group by checking can save models and can save playspaces. When complete, press the add button.

.. image:: _static/asAdmin/15_editGroup.gif
 
Figure 15: Opening the edit group window.

Now, select the joe smith friends group and choose Edit from the Groups menu so that you can add users to the group.

.. image:: _static/asAdmin/16_addUser.gif
 
Figure 16: Adding joe smith and tutorialUser to the group.

Press the view/edit member buttons, and then select joe smith from the list of users and groups on the right of the view/edit members window. Press the left arrow button circled in figure 16 to move joe smith into the members column. joe smith is now a member of the group, as shown in the figure. 
You may also add tutorialUser to the group by repeating the process. 

Step 3: Changing your administrator password.
If the server you are running for this tutorial will remain available to other users who should not have administration permissions, you should change the password for the user root that you are currently logged in under.

.. image:: _static/asAdmin/17_changePassword.gif
 
Figure 17: Changing the password for the user you are currently logged in under.

To do this, first select change password from the Options menu (you could also choose to edit the user root from the users list). This is shown in figure 17.

.. image:: _static/asAdmin/18_editPassword.gif
 
Figure 18: Entering the new password.

Figure 18 shows the the change password window for the user root. Type in the existing password (cadlab) in the old password field and then type in the new password twice, once in each of the two new password fields. Be sure to use a password that you can remember! When you are done press the OK button.

Step 4: Managing server file space. 

As a member of the administrators group, you have access to all of the different users and groups file space on the server. At present the server file management tools are very primitive.

.. image:: _static/asAdmin/19_manageFileSpace.gif
 
Figure 19: Opening the manage file space window.

Select manage file space from the Options menu to open a file space window.

.. image:: _static/asAdmin/20_serverFileSpace.gif
 
Figure 20: View of the server file space.

The window will open displaying the server file space for individual use. If you expand the folders as shown in figure 20, you will recognize this as the file space you navigated in the running models tutorials. The server space is synonymous with the file space provided for the administrators group. Only members of the administrators group can deploy to, and edit within, this file space.
The editing controls are in the bottom right of the window. 
Models, projects and folders can be deleted by first selecting and then pressing the delete button. Folders can be deleted only if they are empty. 
The public and private folders cannot be deleted. Public means that any user with permission will be able to browse the folder in run mode to view content within (provided that permissions for the models/projects within allow the given user to see them). Private means that only members can browse items within the folder.
Only folders can be renamed. This is done by selecting the folder and pressing the rename button. If you want to rename a model, project, or playspace you will need to change the name in build mode and then redeploy.
The folder button in the bottom right is used to add new folders. New folders can be added to folders only. Select the folder you would like to put a new folder into and then press the folder button.

.. image:: _static/asAdmin/21_userFileSpace.gif
 
Figure 21: Switching to the user file space.

As administrator, you can manage the file space of all users on the server. Figure 21 shows what the user file space will look like for a default installation where the tutorials have been followed. You can switch to the user file space using the combination box as shown in the figure.
You may recall there are other users with permission to log into the server (e.g., guest, tutorialUser, root) but these users do not appear in the window. This is because these users did not have can save models or can save playspace permissions. Therefore they do not appear in the file space listing.
You can see joe smith's private directory only because you are logged in as the administrator root.

.. image:: _static/asAdmin/22_addingFolder.gif
 
Figure 22: Adding a folder to joe smith's public folder.

If the Public folder for joe smith is empty (meaning that you have not completed the DOME models tutorials yet), select the folder and click on the folder button. You will be prompted to name the new folder. Give the new folder the name shown in the figure and press the add button. You will be able to use this folder when you complete the DOME modeling tutorials.

.. image:: _static/asAdmin/23_addedFolder.gif
 
Figure 23: The new folder in joe smith's user space.

There will now be a tutorial models folder in joe smith's public directory as seen in figure 23.

.. image:: _static/asAdmin/24_groupFileSpace.gif
 
Figure 24: Switching to the groups file space.

As administrator you can also access the groups file space. If you are following the tutorials on a default installation of the server you will see that there are two groups with file space on the server--administrators and joe smith friends. Expand the administrators group as shown in the figure.
Note that the administrators group file space looks just like the server file space. This is because the server file space is mapped to the space of the administrators group. This is the space that guest users are first directed to when they log into a server using the run mode application.

.. image:: _static/asAdmin/25_collaborativeFileSpace.gif
 
Figure 25: Switching to the collaborative use space.

Figure 22 shows groups with a collaborative use space. Use the view combination box to change to the collaborative use file space. The administrators group is expanded to show the tutorial example playspace that is used in the running models as a user tutorial.
The collaborative use space can be managed in a manner similar to the individual use file space.

.. image:: _static/asAdmin/26_logout.gif
 
Figure 26: Completing the tutorial by logging out.

Close the file management window using the X in the window title bar. Logout as shown in figure 26 to complete this tutorial.