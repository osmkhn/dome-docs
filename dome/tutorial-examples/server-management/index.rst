Server Management
===========================

This tutorial is intended to familiarize you with how to manage your accounts, server file space, and passwords. After completing the tutorial you should be able to use the DOME server mode application as an administrator or as a user.

Tutorials Available: 

1: Server management as an administrator. In this tutorial you will create users, passwords and add users to group. You will create groups and add users groups. Additionally, you will navigate the server file space learning how to add and delete content.

2: Server management as a user. In this tutorial you will change your user password. If you have been provided with file space on the server you will also learn how to add and delete content in your file space.

.. toctree::
   :titlesonly:

   as-administrator.rst
   as-user.rst
