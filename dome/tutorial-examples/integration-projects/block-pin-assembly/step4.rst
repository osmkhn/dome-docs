Step 4: Deploying the Project
==============================

In this step you will move the project and interface files that you saved in step 3 to a server where they can be executed by remote users. 
You will learn how to: check project files into a server using a deploy mode wizard; and assign a variety of project-related use permissions.

 
.. image:: _static/step4/Picture-1.gif

Figure 1: Opening the project deploy wizard.

Switch DOME to deploy mode and select Deploy-->Project, as shown in figure 1. 

.. image:: _static/step4/Picture-2.gif
 
Figure 2: Login in to your server.

Log into to your local server using your username (joe smith is a placeholder username). This step is completed in figure 2. Press the next button to continue with the deployment process.

.. image:: _static/step4/Picture-3.gif
 
Figure 3: Locate the project file in your file space.

Press the browse button and locate the blockAndPinAssembly in your file space, as shown in figure 3.

.. image:: _static/step4/Picture-4.gif
 
Figure 4: Add description.

Add a comment to appear with the project name in run mode. 'My first project' was added in figure 4. Proceed to the next step.

.. image:: _static/step4/Picture-5.gif
 
Figure 5: Select deployed location.

Select the tutorial models folder in your user account. This will be the location of the deployed project on the server. Proceed to the next step.

.. image:: _static/step4/Picture-6.gif
 
Figure 6: Setting project editing permissions.

Set the project editing permissions as shown in figure 6. These permissions are identical to those described in the deploy step of the rectangular model with hole tutorial. Also, concepts related to checking out models/projects/playspaces for editing and redeloyment were covered in step 6 of the rectangular soild with hole tutorial.
Leave yourself with editing permissions and proceed to the next step.

.. image:: _static/step4/Picture-7.gif
 
Figure 7: Select project interfaces to be made available.

Select the assembly costing interface so that it will be available on the server (figure 7). In this case the project has only one interface, but in other cases you may have defined several different interfaces to your project.
Proceed to the next step.

.. image:: _static/step4/Picture-8.gif
 
Figure 8: Setting use permissions for the project interface.

Add guest to the list of users who will be able to see the project interface and use it in run mode. The interface use permissions are identical to those described in the deploy step of the rectangular solid with hole tutorial.
Proceed to the next step.

.. image:: _static/step4/Picture-9.gif
 
Figure 9: Setting permissions on who can see inside of the project.

The running models as a user tutorial illustrated what seeing inside of a project means--that the user can see what resources have been subscribed to, and observe the resource models executing from within the project run window. 
Leave yourself with full permissions to see inside of the project as in figure 9. 
The meaning of the different permissions on this set are as follows:
May set iProject content visibility permissions: Gives permission to edit this permission set.
May set interface use permissions for iModels: Gives permission to edit use permissions on the interfaces of iModels within the project.
May see contents while subscribing: Gives the user permission to see inside the project when it is a resource in another project. This would allow the user to directly subscribe to resources models within the subscribed project (avoiding hierarchical encapsulation).
May see contents and run iProject in new playspace: Allows the user to see inside of the project in run mode, and to execute the project for both individual use and collaborative use.
May see contents and save iProject in new playspace: The functionality to support this permission has not been implemented yet.

.. image:: _static/step4/Picture-10.gif
 
Figure 10: Selecting which integration model interfaces will be available on the server.

The next step is to select which iModel interfaces to make available to project users. In this tutorial, leave the Default interface unchecked since we will did not address iModel interfaces while building the project.
Proceed to the next step.

.. image:: _static/step4/Picture-11.gif
 
Figure 11: Summary of your deployment options.

The final step summarizes the deployment options that you selected, as shown in figure 11. Press the deploy button and in a short period you will receive a confirmation dialog that the deployment was successful.
Your new project is now ready to run.