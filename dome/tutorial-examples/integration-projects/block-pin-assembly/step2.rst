Step 2: Defining Integration Relationships
============================================

In this step you will complete the project iModel started in step 1. 
You will make the mappings needed to keep the block and the pin in parametrically consistent states--as if they are part of an assembly.
Additionally, you will define a relationship to calculate the cost of the assembly based upon the cost of the sub-components.
The process will be the same as for building DOME models.
 
.. image:: _static/step2/Picture-1.gif

Figure 1: Adding a real parameter to the iModel.

Your view of the project's integration model should be the same as in figure 1. 
The first thing you will do is connect the diameter used by the pin and the hole diameter used by the block so that they will be the same. Since no intermediate calculations are needed between the two parameters, you will be able to use mappings.
However, since input-input mappings are not supported by the DOME solver currently, you will need to create a model parameter to drive them, just as you did in the Rectangular model with hole tutorial (figures 1-9).
Choose Add-->Real to put a new parameter into the model as shown in figure 1.
Note: When you add the parameter, elements of neither interface should be selected or you will get a 'cannot paste' dialog. Clicking on the white space at the bottom of the window will deselect the interfaces.

.. image:: _static/step2/Picture-2.gif

Figure 2: Name the new parameter.

Rename the model parameter (at the bottom of figure 2) to 'hole diameter'. Give the parameter the unit centimeter and a default value of 4.

.. image:: _static/step2/Picture-3.gif

Figure 3: Coping the hole diameter into the clipboard.

With the hole diameter parameter selected, choose the Edit-->copy menu item to add it to the clipboard, as shown in figure 3.

.. image:: _static/step2/Picture-4.gif
 
Figure 4: Hole diameter mapped to the pin interface diameter.

Select the diameter in the pin interface and then choose the Add-->Map menu item. The hole diameter and pin diameter will now be mapped as in figure 4.

.. image:: _static/step2/Picture-5.gif 

Figure 5: Mapping the block's hole diameter.

Now, select the diameter in the volume and cost interface of the block. Choose the Add-->Map menu item and the block diameter will also be mapped to the hole diameter parameter, as highlighted in figure 5.

.. image:: _static/step2/Picture-6.gif 

Figure 6: Graph visualization of the model.

The graph visualization is useful to check the structure of the model. Use the visualization combination box to switch to the graph as shown in figure 6. You can see how the hole diameter drives the diameter in both of the subscribed interfaces.

.. image:: _static/step2/Picture-7.gif

Figure 7: Copying the block height.

Switch back to the model list visualization. 
Now, you will work on making the length of the pin match the h of the block. 
Add another real parameter to the model (using the Add menu) and name it block thickness, as shown in figure 7. Set its unit and default value to match the figure.

.. image:: _static/step2/Picture-8.gif

Figure 8: Coping the block thickness into the clipboard.

Select the block thickness and copy it into the clipboard as shown in figure 8.

.. image:: _static/step2/Picture-9.gif

Figure 9: Fully mapped block thickness.

Select the length parameter in the pin interface and choose Add-->Map.
Select the h parameter in the block volume and cost interface and choose Add-->Map.
You should now see mappings as highlighted in figure 9.

.. image:: _static/step2/Picture-10.gif

Figure 10: The new model graph.

Switch to the graph visualization to verify the model structure, as shown in figure 10.

.. image:: _static/step2/Picture-11.gif

Figure 11: Adding an assembly cost relation to the model.

Switch back to the model list visualization. Choose Add-->Procedural relation to add a relation to the model. Rename it to match figure 11. 

.. image:: _static/step2/Picture-12.gif

Figure 12: Coping the pin cost into the relation.

Select the pin cost and copy it into the clipboard as shown in figure 12.  

.. image:: _static/step2/Picture-13.gif

Figure 13: Adding and mapping pin cost into the relation.

Now, select the assembly cost relation and choose Add-->Add and Map to add a relation parameter and map it to the pin cost in one step. You will see the mapping information appear in the mapping column. 

.. image:: _static/step2/Picture-14.gif

Figure 14: Rename the relation parameter.

Rename the relation parameter to pin cost as shown in figure 14. 
Note that after entering the change, the mapping information for the parameter cost in the pin interface will update to the new relation parameter name.

.. image:: _static/step2/Picture-15.gif

Figure 15: Adding the block cost to the relation.

Now, select the cost parameter in the block volume and cost interface.
Use the Edit-->copy menu to put the selected parameter into the clipboard.
Select the assembly cost relation, and then use the Add-->Add and Map menu item to add a new parameter to the relation and map it to the cost in the block's interface in a single step.
Finally, as shown in figure 15, rename the new relation parameter from cost1 to block cost.

.. image:: _static/step2/Picture-16.gif

Figure 16: Graph view of the model structure.

Switch back to the graph visualization to verify the model structure. Your graph should look like figure 16.

.. image:: _static/step2/Picture-17.gif

Figure 17: Adding additional parameters to the relation.

Switch back to the list visualization, select the assembly cost relation, and then add a new real parameter to it using the Add menu, as shown in figure 17.

.. image:: _static/step2/Picture-18.gif

Figure 18: Complete set of relation parameters.

Repeat the process to add a second new parameter to the relation. Name the two new parameters as shown in figure 18. Set the default value for the assembly labour and assign dollar units to both parameters.

.. image:: _static/step2/Picture-19.gif

Figure 19: Defining the body of the relation and its causality.

Double click on the assembly relation icon to open its editor window. Complete the equation as shown in figure 19. Also, define the causality for the relation using the causality editor.
Test the relation, and then close the relation window.

.. image:: _static/step2/Picture-20.gif

Figure 20: Graph of the model.

Once again, switch to the model graph visualization. The structure of your graph should match figure 20.

.. image:: _static/step2/Picture-21.gif

Figure 21: The completed integration model.

Switch the model back to the list visualization and collapse the subscribed interfaces to match figure 21. You are now ready to add project interfaces.
You may want to save the project at this point.