Step 5: Running the Project
=============================

In this step you will run the new project that you deployed in step 4.
This has been covered in the running published models tutorials so it is not duplicated here.
 
.. image:: _static/step5/Picture-1.gif

Figure 1: The project viewed in the run browser.

Switch to the run mode application and log into your user account. When you navigate to your tutorial models folder, you should see the new project as shown in figure 1.
When you run the project you may want to try the project interface in the Build View that you created when making the interface. 
If you start the project by opening the project window (double click on the project icon), you will be able to see changes propagating through the resource models while the project is executing.
If you log in as guest and navigate to your user space, you will only be able to see the project interface.
You have now completed the block and pin assembly tutorial.