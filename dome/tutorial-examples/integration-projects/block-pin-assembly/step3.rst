Step 3: Adding Interfaces
=============================

In this step you will create an interface for the project that you completed in step 2. 
The interface will expose parameters in the project iModel for use in run mode.
The actions in this step are very similar to adding interfaces to a DOME model, as in step 3 of the rectangular solid with a hole tutorial.
 
.. image:: _static/step3/Picture-1.gif

Figure 1: Opening the project interface tool.

To begin, use the Tool-->Project Interfaces menu item to open the project interface tool, as shown in figure 1.
Note: Integration models can also be provided with interfaces, but you will not be working with these in this tutorial.

.. image:: _static/step3/Picture-2.gif
 
Figure 2: Renaming the new project interface.

Use the Interfaces-->New menu item to add a new project interface. Rename the interface to match figure 2--'assembly costing interface'.
Double click on the interface icon open the interface's window.

.. image:: _static/step3/Picture-3.gif
 
Figure 3: Renaming a context in the interface.

With the interface window in the foreground, use the Add-->context menu to add a context to the interface. Rename the context as shown in figure 3. 

.. image:: _static/step3/Picture-4.gif
 
Figure 4: Selecting user changeable design variables.

Bring the integration model window into the foreground, and switch the view to Model Causality View as highlighted in figure 4.
Select the independent parameters hole diameter and block thickness and copy them into the clipboard. This is shown in figure 4. Use <ctrl><shift> for multiple selection.

.. image:: _static/step3/Picture-5.gif
 
Figure 5: Adding the the independent parameters to the interface.

Bring the interface window back to the foreground. Select the design variables context, and then choose Add-->Add and Map-->Last selection as shown in figure 5. This will add parameters to the interface and map them back to the parameters in the model.

.. image:: _static/step3/Picture-6.gif
 
Figure 6: Newly added interface parameters.

Your interface should now look like figure 6. 

.. image:: _static/step3/Picture-7.gif
 
Figure 7: Adding a context to the interface.

Use the Add menu to add another context to the interface. Name the context 'block configuration' as has been done in figure 7.

.. image:: _static/step3/Picture-8.gif
 
Figure 8: Copying the block parameters into the clipboard.

Bring the integration model window back into the foreground and switch back to Build View, as highlighted in figure 8. 
Expand the block volume and cost interface and select the parameters diameter, h, width, and length. Use the Edit menu to copy them into the clipboard.

.. image:: _static/step3/Picture-9.gif
 
Figure 9: Adding the block parameters to the interface.

Bring the interface window back into the foreground, select the block configuration context, and then Add and Map the block parameters into the interface, as shown in figure 9.

.. image:: _static/step3/Picture-10.gif
 
Figure 10: Rename the block interface parameters.

For clarity, rename the block interface parameters as shown in figure 10.

.. image:: _static/step3/Picture-11.gif
 
Figure 11: Adding another context to the interface.

Now, add another context to the interface. Name it 'pin configuration', as in figure 11.

.. image:: _static/step3/Picture-12.gif
 
Figure 12: Selecting the pin parameters.

Return to the model window and expand the pin interface subscription, as shown in figure 12. Select the length, diameter, head diameter, and head height parameters and copy them into the clipboard.

.. image:: _static/step3/Picture-13.gif
 
Figure 13: Add pin parameters to the interface and rename them.

Return back to the interface window and, with the pin configuration context selected, use the Add and Map command to add the selected pin parameters to the interface.
Rename the parameters as shown in figure 13 for clarity.

.. image:: _static/step3/Picture-14.gif
 
Figure 14: Add a cost parameters context to the interface.

Add a final context to the interface and name it 'cost parameters', as shown in figure 14.

.. image:: _static/step3/Picture-15.gif
 
Figure 15: Selecting the cost parameters from the model.

Return back to the model window, expand the assembly cost relation and select the pin cost, block cost, and assembly cost. Then, use the Edit menu to copy the selection into the clipboard. This is shown in figure 15.

.. image:: _static/step3/Picture-16.gif
 
Figure 16: Cost parameters added to the interface.

Return back to the project interface window and select the cost parameters context. Use the Add and Map command to add the selected cost parameters to the interface, as has been done in figure 16.
Save the project. The project is now complete and ready for deployment.