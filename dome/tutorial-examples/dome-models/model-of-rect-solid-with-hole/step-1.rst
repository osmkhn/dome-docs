Step 1: Starting Model and Building Relations
===============================================

In this step you will use build mode to start a new DOME model and implement two relations, one to calculate the block width and length as a function of the hole diameter, and another to calculate the volume of the solid.
You will learn how to: add parameters (real numbers); set units; and both add and test relations.

.. image:: _static/step1/Picture-1.gif

Figure 1: Changing to run mode.

Start the DOME client application. You do not need a server running until later steps in the tutorial when your model is deployed. Make sure that the client is in build mode, as shown in figure 1.

.. image:: _static/step1/Picture-2.gif
 
Figure 2: Starting a new model.

Use the Build-->New model-->Dome Model menu item to open a new model (figure 2).

.. image:: _static/step1/Picture-3.gif
 
Figure 3: Changing the model name.

Click in the name field and change the name of the model from the default Dome Model to Rectangular solid with hole, as shown in figure 3. This is the model name that you will see in the run browser (in run mode).

.. image:: _static/step1/Picture-4.gif
 
Figure 4: Adding a procedural relation.

Now, add a procedural relation to the model. Use the Add-->Procedural Relation menu item as shown in figure 4.

.. image:: _static/step1/Picture-5.gif
 
Figure 5: Adding real parameters to the relation.

Next, you will add real parameters to the relation. Select the relation by clicking on its icon, and then add a parameter using the Add-Real menu item as shown in figure 5. A relation parameter should then appear in the input filter. 
Note: If the parameter appears outside of the relation (not in the input filter), this means that you forgot to select the relation before adding the parameter. You can delete this parameter by first selecting it (click on its icon) and then choosing Edit-->Delete.

.. image:: _static/step1/Picture-6.gif
 
Figure 6: Relation containing three parameters.

Add two more real parameters to the relation so that you now have three real parameters in the input filter, as shown in figure 6.

.. image:: _static/step1/Picture-7.gif
 
Figure 7: Changing the parameter names.

Rename each of the parameters as shown in figure 7. When you click on the parameter's name it will become editable. When you leave the cell or hit return the change will be entered.
Note: Any object in a DOME model, except parameters that are inside of a relation, can have spaces in their name. 

.. image:: _static/step1/Picture-8.gif
 
Figure 8: Adding units to the width parameter.

Next, add units to the width parameter. When you click on the value column cell for the width, a unit combination box will appear. Use the combination box to select change unit, as in figure 8.
Note: When working with physical quantities is a good idea to assign units so that you can take advantage of DOME's dimensional analysis checks in relationships and automatic unit conversion. These features help prevent modeling errors.

.. image:: _static/step1/Picture-9.gif
 
Figure 9: Selecting length units.

The unit chooser will appear as shown in figure 9. Select the length category from the list of dimensions on the left of the chooser. Several length units will then appear in the units list on the right. Use the combination box below the units list to filter only for metric units.

.. image:: _static/step1/Picture-10.gif
 
Figure 10: Selecting a millimeter unit.

Select millimeter, as shown in figure 10, and then click ok to dismiss the unit chooser. 

.. image:: _static/step1/Picture-11.gif
 
Figure 11: Units added to all parameters in the relation.

Add length units to the other two parameters in the relation, as shown in figure 11. Note that holeDiameter is in centimeters.

.. image:: _static/step1/Picture-12.gif
 
Figure 12: The relation editor GUI.

Open the relation's editor GUI by double clicking on the relation icon, as shown in figure 12.

.. image:: _static/step1/Picture-13.gif
 
Figure 13: Using the relation GUI to define the the width and length equations.

Enter the equations for the width and length in the relation body area as shown in figure 13. The body editor has a number of convenient features. In particular, you can type the first character of a parameter name and the use <ctrl><shift> to auto complete the rest of the name.
Note: The body of the relation is written in Python. A complete listing of operations available for each type of parameter is in the methods and operators section of the reference documentation.

.. image:: _static/step1/Picture-14.gif
 
Figure 14: Defining relation causality

You also need to define the relation's causality so that DOME can correctly solve sets of many relations. 
Click on the relation causality tab, as highlighted in figure 14 (1). Then, click on the edit causality button (2). The edit causality window will appear. Use the check boxes to indicate that the width depends on the holeDiameter and that the length also depends on holeDiameter, as has been done in figure 14.
When you are done click the ok button.
Note: A more detailed explanation of how to read the causality editor is in the in the causality editor reference documentation.

.. image:: _static/step1/Picture-15.gif
 
Figure 15: Correctly completed causality information.

After closing the edit causality window the causality tab will appear as in figure 15.

.. image:: _static/step1/Picture-16.gif
 
Figure 16: Setting an initial value for the hole diameter.

Return to the view of relation parameters by clicking on the interface parameters tab highlighted in figure 16. Note that the relation parameters are now sorted into both the input and output filters.
Click on the value column cell for the hole diameter and enter a value of 1.0. The value is entered when the cell background changes back to white.

.. image:: _static/step1/Picture-17.gif 

Figure 17: Testing the relation.

It is always good practice to test relations as you go. Use the Edit procedural relation-->Test menu item as shown in figure 17. 
Note: If this menu is not available, the relation GUI is not in focus. Click on the relation GUI or use the Windows menu to bring it into the foreground.

.. image:: _static/step1/Picture-18.gif
 
Figure 18: Successfully tested relation.

After a short moment, the output values of the relation should update as shown in figure 18. Note that DOME has handled the centimeter to millimeter conversions. 
If there are unit related problems with your relation, a dialog will appear and you should edit the units accordingly. If nothing seems to happen, there is probably a syntax error in the relation. Syntax errors are directed to the message log. The message log button is in the main model window. Carefully check you relation body and causality information in comparison to the figures in the tutorial.

.. image:: _static/step1/Picture-19.gif
 
Figure 19: Changing the relation name.

Finally, change the name of the relation to something more meaningful, as shown in figure 19. Use the X in the relation window title bar to close the window.

.. image:: _static/step1/Picture-20.gif
 
Figure 20: Adding a second relation.

Use the Add-->Procedural Relation menu item to add a second relation to the model. Change the name of the relation to volume relation, as has been done in figure 20.

.. image:: _static/step1/Picture-21.gif
 
Figure 21: Adding parameters to the relation and setting their units.

With the volume relation selected as shown in figure 21, use the Add-->Real menu item to add 5 real parameters to the relation. 
Change the names of the parameters to match the figure, and assign units to the parameters. Note that the vol parameters has been assigned the volume unit cubic millimeter.

.. image:: _static/step1/Picture-22.gif
 
Figure 22: Adding a real parameter using the relation context menu.

Double click on the volume relation icon to open its editor GUI, as shown in figure 22. Use the Add menu as shown in the figure to insert a sixth real parameter into the GUI.

.. image:: _static/step1/Picture-23.gif
 
Figure 23: Defining the constant PI.

Change the name of the new parameter to PI and set its value to 1.0, as shown in figure 23. Then use the constant category of the unit chooser to assign the parameter the unit of PI. In an equation, the value of 1.0 PI will be the correct 3.14...
Note: If you cannot find PI in the constant list, this is probably because the unit filter is set to metric. Change the combination box to show all units, as is the case in figure 23.

.. image:: _static/step1/Picture-24.gif
 
Figure 24: Define the body of the relation and the relation causality.

Now, define the volume equation in the relation body and the causality of the relation as shown in figure 24, following the same process you used to define the width/length relation.

.. image:: _static/step1/Picture-25.gif
 
Figure 25: Testing the procedural relation.

Change input parameters to non-zero values. Remember that the PI parameter must remain 1.0 for the equation to be correct. Test the relation as shown in figure 25.

.. image:: _static/step1/Picture-26.gif 

Figure 26: The tested relation.

If there were no errors, the value of the volume is now calculated. If there are errors, correct units or equation syntax accordingly. Check the message log using the message log button in the main model GUI if necessary.
Close the volume relation GUI.

.. image:: _static/step1/Picture-27.gif
 
Figure 27: The two completed relations shown in the mode GUI.

The two relations should now appear in the model GUI as shown in figure 27. You have now completed step 1 of the tutorial.