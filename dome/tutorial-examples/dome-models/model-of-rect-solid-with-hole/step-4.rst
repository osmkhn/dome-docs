Step 4: Deploying and Setting Permissions
==========================================

In this step you will move the model and interface files that you saved in step 3 to a server where they can be executed by remote users. 
You will learn how to: check model files into a server using a deploy mode wizard; select which interfaces will be available to users; and assign use permissions.

 
For this step you will need to have a server running on which you have a user account providing you with file space. This was addressed in the prerequisites for DOME modeling tutorials.
The tutorial is written assuming that you will be logging into a server running locally on the same machine as your client application. Recall that the account for joe smith is used as a tutorial placeholder for your account.

.. image:: _static/step4/Picture-1.gif

Figure 1: Switching the client application to deploy mode.

To begin, switch the client application to Deploy, as shown in figure 1. 

.. image:: _static/step4/Picture-2.gif
 
Figure 2: Starting the Model deploy wizard.

Select the Deploy-->Model... menu item to start the model deployment wizard, as shown in figure 2.

.. image:: _static/step4/Picture-3.gif
 
Figure 3: Logging in to a server.

The deploy wizard will step you though a series of choices that need to be completed during the deployment process. 
The first step, shown in figure 1, is to log into a server on which you have an account with file space for saving models. 
Click on the highlighted login button in the main panel of the wizard to open the login window as shown in figure 3. Log in to your account on the local machine (remember, joe smith is a placeholder for your user name, as was defined in the server management as an administrator tutorial. The tutorial user joe smith has a password 123). 
Unlike the login window in the figure 3, you may also have cached logins available in the login window.

 
.. image:: _static/step4/Picture-4.gif

Figure 4: Successfully completed login.

After you have logged into the server, the wizard will display a summary of your login in information as shown in figure 4. 
With the login complete, the next button is enabled so that you can proceed with the deployment process.

.. image:: _static/step4/Picture-5.gif
 
Figure 5: Choose the model file saved on your computer.

Use the browse button in the select model panel to open a file chooser. Navigate to the model file you saved in step 3 and select it, as shown in figure 5.

.. image:: _static/step4/Picture-6.gif
 
Figure 6: Adding a model description.

You can also add a model description as shown in figure 6. This description will appear in the run browser beside the model name.
Press the next button to continue.

.. image:: _static/step4/Picture-7.gif
 
Figure 7: Selecting the location in your file space on the server.

Select the folder in which you want the model to be located on the server. Select the folder tutorial models as shown in figure 7.
If you did not complete the file space management portion of the server management as an administrator tutorial, you will probably not have a folder called tutorial models. If this is the case, select your Public folder and then click the add folder button (highlighted in figure 7). Enter the folder name tutorial models.
With the destination folder selected, you may now proceed to the next step.

.. image:: _static/step4/Picture-8.gif
 
Figure 8: Setting permissions for who can edit the model.

Now, you will be asked to define editing permissions for the model, as shown in figure 8. As owner of the model, you are automatically given all editing permissions. 
Editing permissions pertain to being able to check the model out from the server, edit the model's content and then redeploy it back to the server or other servers.
Set model or iProject edit permissions: provides permission to decide who can set edit permissions. If you do not have this permission you will not be able to set any of the permissions in this list.
Delete model or iProject: provides permission to delete the deployed model from the server during server file space management.
Modify model or iProject: provides permission to check out the model from the server for editing, and to redeploy the edited model onto the server as a version update of the original model.
Copy model or iProject: allows the user to check out the model, but they cannot redeploy it as a version update to the original model. They can only deploy it as a different model. Since copy is a subset of modify, this is always available when modify permissions are available.
Set interface use privileges: This is available only if you have modify or copy permissions. This gives you permission to decide which users can use what model interfaces in run mode.
Leave yourself as the only user with full editing permissions and proceed to the next step. 

.. image:: _static/step4/Picture-9.gif
 
Figure 9: Choosing which interfaces will be available.

The next step, shown in figure 9, is to determine which interfaces will be made available to users. Select the volume interface for your model and proceed to the next step.

.. image:: _static/step4/Picture-10.gif
 
Figure 10: Assigning which users may use the interface.

Now, as shown in figure 10, you can define which users will be allowed to see and use available model interfaces. 
The combination box allows you to select which of the available interfaces you are setting use permissions for. In this case there is only one interface available (volume interface). 
As model owner you are automatically given full use permissions.
Subscribe to interface: allows the interface to be used as a sub-model when you are building an integrated project.
View and run in a new playspace: allows use of the interface for individual and collaborative use, as illustrated in the running models tutorials.
Save in a new playspace: the current implementation does not yet have the functionality related to this permission.

.. image:: _static/step4/Picture-11.gif
 
Figure 11: Provide guest with use permissions.

Leave yourself with fill use permissions, and add guest as a user for the interface as shown in figure 11.
Leave guest with full use permissions and proceed to the next step.

.. image:: _static/step4/Picture-12.gif
 
Figure 12: Summary of deployment choices.

A summary of your deployment choices will be displayed. If you change your mind, you can use the back button to go back through the wizard and make modifications.
Press the deploy button to initiate the transfer of the model and interfaces to the server.

.. image:: _static/step4/Picture-13.gif
 
Figure 13: Completion of the deployment process.

When the file has been transferred to the server you a status dialog will appear as shown in figure 13. Dismiss the dialog.
The model is now available for use in run mode.