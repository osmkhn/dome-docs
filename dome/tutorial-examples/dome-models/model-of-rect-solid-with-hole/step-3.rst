Step 3: Building Interfaces
=============================

In this step you will create an interface for the model that you completed in step 2. Interfaces are the exposed views of a model made available to run-mode users.
You will learn how to: use the interface tool; create a new interface; use context to organize the interface; add parameters to the interface; and save models.
 
.. image:: _static/step3/Picture-1.gif

Figure 1: Opening the Interfaces tool.

If you just completed step 2, you will have the model open and showing the Model Causality View for the list visualization, as in figure 1.
Now, open the Interfaces tool using the Tools-->Interfaces menu item.

.. image:: _static/step3/Picture-2.gif
 
Figure 2: Creating a new interface.

When the Interface tool window opens, as in figure 2, you will see an interface called Default interface. This is an auto-generated interface that contains model parameters that are in the Independent or Results filter of the Model Causality View. 
In this tutorial we will build our own interface from scratch rather than using the default interface. All models can have multiple interfaces.
With the interface tool in the foreground, use the Interfaces-->New menu item to create a new interface.

.. image:: _static/step3/Picture-3.gif
 
Figure 3: The newly created interface.

The newly created interface will appear in the interface tool window, as shown in figure 3. Double click on the interface Icon to open an editing window for the new interface.

.. image:: _static/step3/Picture-4.gif
 
Figure 4: The interface editing window.

The interface editor window will appear in its build view, as shown in figure 4. Start by giving the interface a new name, such as volume interface.

.. image:: _static/step3/Picture-5.gif
 
Figure 5: Adding a context to the interface.

With the interface window in the foreground, select the Add-->Context menu item. You can think of context as file folders--we will use them to organize the parameters that we put into the interface. 

.. image:: _static/step3/Picture-6.gif
 
Figure 6: Two context have been added to the interface.

Add a second context to the interface. Name them as shown in figure 6. When you are done, click on the free dimensions context to select it.

.. image:: _static/step3/Picture-7.gif
 
Figure 7: Selecting independent model parameters that can be user inputs.

Next, bring the model window into the foreground by either clicking on it or using the Windows menu. 
Use <ctrl><click> to multiple select both the diameter and the block height h. Do not select PI, as this is something that you do not want users to be able to change. Then, as shown in figure 7, select the Edit-->copy menu item to place the two parameters into the clipboard.

.. image:: _static/step3/Picture-8.gif
 
Figure 8: Adding the paramameters into the interface.

Now, you will add copies of the chosen model parameters to the interface, and then map them to the corresponding data in the model.
Bring the interface window back into the foreground, as shown in figure 8. Select the free dimensions content, and then choose Add-->Add and Map-->Last selection from the menu.

.. image:: _static/step3/Picture-9.gif
 
Figure 9: The new interface parameters mapped to the corresponding parameters in the model.

Figure 9 shows the new interface parameters that were added. Interface parameters are green. 
The interface parameters are separate copies of the parameters in the model. However, they have mappings to the original parameters in the model (note the mapping column in figure 9). Thus, the interface and model parameter will mirror the same values in when the model is running. 
If desired, you can change the names of the interface parameters, or even their units, so long as the new units are dimensionally consistent with the model parameter to which they are mapped (e.g., changing centimeters to inches).
Note: If the parameters did not appear inside the context, it means that the context was not selected. You can delete the interface parameters and repeat the Add and Map with the context selected.

.. image:: _static/step3/Picture-10.gif
 
Figure 10: Selecting intermediate variables for the interface.

Bring the model window into the foreground once again. Select the intermediate variables length and width as shown in figure 10. Use the Edit-->copy menu item to place this selection into the clipboard.

.. image:: _static/step3/Picture-11.gif
 
Figure 11: Adding the intermediate parameters to the interface.

Now, bring the interface window into the foreground once again. Select the driven dimensions context, and then choose the Add-->Add and Map-->Last selection menu item. The new length and width interface parameters will now appear in the interface.

.. image:: _static/step3/Picture-12.gif
 
Figure 12: Adding vol to the interface.

Finally, return to the model and copy the vol parameter into the clipboard. Return to the interface window and add and map the interface parameter. The result is shown in figure 12.
Optional step: Interfaces are designed to hide the model implementation from users at run-time. However, there are times that you might want to see the insides of an executing model, especially while debugging. There is a read-only model view intended for this purpose. If you would like to expose the relations used in this model, first return to the model window and switch to the Object Type view. Select the two relations and then copy them into the clipboard. Bring the interface window back into the foreground. Click on the model view button (the button with 3 cubes to the right of the interface name) and the model view window will open. With the model view in the foreground, select the Add-->Add and Map-->Last selection menu item. Mapped copies of the relation will appear in the model view.

.. image:: _static/step3/Picture-13.gif
 
Figure 13: Saving the model and interfaces.

The final step is to save your model on your local file space. The writes the model and interfaces out into an XML file. Use the DOME Model-->Save menu item shown in figure 13.

.. image:: _static/step3/Picture-14.gif
 
Figure 14: Giving the model file a name.

You may want to make a directory for your model since a number of files will be saved to disk. You will be asked to give a name for the model file. The file name is just used on your local file system. The model name defined in the model window (Rectangular solid with hole) is the name that will be seen when the model is available in run mode.