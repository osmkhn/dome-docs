Step 6: Revising the Model
===========================

In this step you will edit the model that you deployed in step 4 by adding a cost calculation. This will be done in a way that lets you redeploy the changes as a version update in step 7.
You will learn how to: understand the DOME version control system; open and edit an existing model; and save the model so that it can be redeployed.
The step assumes that you are comfortable will defining relations in DOME models (step 1), making mappings (step 2), and adding parameters to interfaces (step 3).
 
.. image:: _static/step6/Picture-1.gif

Figure 1: The folder containing the model files on your computer file space.

We will begin with an explanation of DOME version control.
Navigate to the folder where you saved your DOME model in step 3. You will see something similar to figure 1 (In our case the model was saved in a folder called DOME models). 
When you saved the model in step 3, the model file rectangularSolid-DOME.dml was created. The directory containing the model interface files was also created. You will also note that there is a folder called domeVC (VC for version control). This folder was created when you deployed the model to the server in step 5 . In this folder there is information that will allow DOME to determine when you can redeploy/update models already published on a server, and whether the version on the server has changed since you last deployed or checked out the model.
Without the domeVC information it is not possible to redeploy version updates. 

.. image:: _static/step6/Picture-2.gif

Figure 2: Checkout model option (not required in this tutorial)

Since you built and deployed the model on the same computer that you are using now, you are ready to begin editing the model. 
However, if you were now on a different computer, you would need to checkout the model from the server. Checkout is available in build mode, as highlighted in figure 2. The checkout process will download copies of the model files from the server to your computer and also generate the necessary DOME version control information.

.. image:: _static/step6/Picture-3.gif
 
Figure 3: Opening the model.

Open your model by choosing the Build-->Open model...->Dome Model menu item.

.. image:: _static/step6/Picture-4.gif
 
Figure 4: Select the model file.

Navigate to the folder containing your model and select it. Note that the file chooser filters out the interface folders and the domeVC folder so that it is easy to locate the model file.

.. image:: _static/step6/Picture-5.gif
 
Figure 5: Adding a new relation to the open model.

The model will open as shown in figure 5. You will be adding a cost calculation to the model. 
Add a procedural relation to the model using the Add menu.

.. image:: _static/step6/Picture-6.gif
 
Figure 6: Define the cost relation.

Rename the relation and add three real parameters to the cost relation. Name the parameters as shown in figure 6. Don't forget to assign units to the parameters and a value to the costPerVolume.
Complete the equation shown in the relation body.

.. image:: _static/step6/Picture-7.gif
 
Figure 7: Define the causality for the relation.

Define the relation causality by selecting the causality tab and opening the edit causality information dialog as shown in figure 7. 
Test the relation (in the Procedural relation edit menu) and then close the relation GUI.

.. image:: _static/step6/Picture-8.gif
 
Figure 8: Copying the vol parameter into the clipboard.

Expand the volume relation in the model window as shown in figure 8. Select the vol parameter and copy it into the clipboard as shown.

.. image:: _static/step6/Picture-9.gif
 
Figure 9: Mapping vol to volume.

Select the volume parameter, and then Add-->Add and Map-->Last selection to make the mapping (figure 9).

.. image:: _static/step6/Picture-10.gif
 
Figure 10: Completed mapping between the volume and vol parameters.

Your model should now appear as shown in figure 10. You can double click on a cell in the mapping column if you need to use the mapping tool to edit the mapping.
Changes to the model are now complete.

.. image:: _static/step6/Picture-11.gif
 
Figure 11: Copying the cost parameter into the clipboard.

The final step will be to add the cost variable to an interface. First, select the cost parameter and copy it into the clipboard as shown in figure 11.

.. image:: _static/step6/Picture-12.gif
 
Figure 12: Adding cost to the interface.

Use the Tool menu to open the interface tool. In this case you will add the cost parameter to the existing volume interface. One could also create a duplicate interface and leave the original volume interface unchanged.
Double click on the volume interface to open it in a separate window. Then choose Add-->Add and Map-->Last selection to add a copy of the cost parameter to the interface and map it to the cost parameter in the model. This is shown in figure 12.

.. image:: _static/step6/Picture-13.gif

Figure 13: Rename the interface.

The interface will now appear as shown in figure 13. Rename the interface to volume and cost interface.

.. image:: _static/step6/Picture-14.gif
 
Figure 14: Save your changes.

Use the Save menu option to save your changes.
Note: If you chose Save as..., you will not be able to redeploy the modified model as a version update. This is because all DOME model objects must have unique identities. Thus, during Save as..., the model, interface, and interface objects are all given new identities. If you want to keep a backup of the original model that can be redeployed, you should make a copy of the folder containing the model file, interfaces, and domeVC folder.
