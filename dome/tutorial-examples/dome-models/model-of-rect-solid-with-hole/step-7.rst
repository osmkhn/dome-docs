Step 7: Redeploying Updated Version
====================================

In this step you will redeploy the model edited in step 6. This will replace the model available on the server with the newer version.
This step is very similar to step 4 (deploying the model). 
 
.. image:: _static/step7/Picture-1.gif

Figure 1: Opening the deploy model wizard.

Switch to build mode and open the deploy model wizard as shown in figure 1.

.. image:: _static/step7/Picture-2.gif

Figure 2: Log in to the server where the model was originally deployed.

Log into the model where the model was originally deployed. You will need to log in as the same user as when you deployed the model (figure 2).
Press the next button when you are logged in.

.. image:: _static/step7/Picture-3.gif

Figure 3: Selecting the model file.

Use the file browser to select the model that you saved on your computer, as shown in figure 3. 

.. image:: _static/step7/Picture-4.gif

Figure 4: Selecting the redeploy option.

Select the redeploy option in the Select model panel (figure 4). The deploy wizard will then contact the server and use your domeVC information to see where the model can be redeployed. If there is more than one possible location you will be asked to pick from the list of options. (A model can be deployed in several different locations if desired)
User and permission information on the server will be downloaded into the wizard so that you do not need to redefine all of this information, although you may edit the information. In figure 4, you can see that the model description has been retrieved from the server.
Proceed with the next button.
Note: If you select redeploy and DOME determines that you cannot redeploy the model onto the chosen server, the redeploy model radio button will automatically switch to deploy as new model. You will also receive a warning if there is a newer version deployed on the server.

.. image:: _static/step7/Picture-5.gif

Figure 5: Model location.

When the Select location panel appears, the model location will already be selected to where the model was previously located. Since you are redeploying, you cannot change this location.

.. image:: _static/step7/Picture-6.gif

Figure 6: Summary of deployment options.

Press next through the permissions steps until you reach the final summary panel. The permissions will be retained as defined in the original deployment. 
Finally, use the redeploy button to update the model on the server.

.. image:: _static/step7/Picture-7.gif

Figure 7: The updated model in run mode.

Switch to run mode, login if needed, and navigate to the model as shown in figure 7. Note that, since both the model and the interface were changed, their version numbers have been increased.
You may run the model as desired.
This completes the final step of the Rectangular Solid with Hole tutorial.
