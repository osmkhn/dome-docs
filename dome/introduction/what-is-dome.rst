What is DOME
---------------

There are three key aspects of the DOME concept and DOME3 implementation.

**A new philosophy of how to build integrated networks:** Just as the hypertext concept underlies the WWW's model for building information networks, the concept of distributed, declarative, emergent system synthesis anchors DOME as our vision for a World-Wide-Simulation-Web (WWSW). 

**An architectural representation that supports the integration philosophy:** In order to realize the hypertext concept in the WWW, a number of enabling protocols and standards were needed, such as http, html, xml, etc. Likewise, DOME3 exploits many of these same protocols and standards, but also includes an object representation necessary to support the WWSW and system simulation. 

**A software implementation to allow the use of the integration philosophy:** In order to use the hypertext concept/WWW several applications are required: authoring tools to generate content (e.g., DreamWeaver, FrontPage); tools to move html files to web accessible computers (e.g., FTP applications), tools that serve html to remote users in the WWW (e.g., Apache, IIS), and tools to view/browse the content of the WWW (e.g., Safari, Netscape, Internet Explorer). DOME3 provides a precisely analogous set of software applications: build; deploy; server; and run.