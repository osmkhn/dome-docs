Design Tools
-------------

The core proposition of DOME is to reduce barriers associated with creating integrated simulations. However, the purpose of developing integrated simulations is to explore the characteristics of alternatives , and there are many different kinds of analysis tools that can facilitate such exploration.  DOME allows other analysis applications to be wrapped with interfaces so that they can interact with DOME-integrated simulations. Tools for system optimization, simulation structure analysis and tradeoff analysis have been developed.