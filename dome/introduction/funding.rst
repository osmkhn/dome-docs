Funding
---------------

The DOME project is grateful for the research funding provided by its sponsors. The initial seeds funds for the project were provided in 1996 by the `Alliance for Global Sustainability (AGS) <http://lfee.mit.edu/programs/ags>`_ and the `Leaders for Manufacturing Program <http://lfmsdm.mit.edu/index.html>`_. `Ford Motor Company <http://www.ford.com/>`_ and  `the Center for Innovation in Product Development <http://web.mit.edu/cipd>`_ have been very strong supporters since 1998. Additional support has been received from LG Electronics, and Bose.