Software Upgrades  
===================

DOME3 uses JAVA Web Start to keep your installation up-to-date. When you start DOME3 and your computer is connected to the Internet, the CADlab server is contacted to check if there is a newer version that should be incorporated into your installation.  A simple click-of-a-button will download and install the software upgrade. Keeping your installation current is recommended.