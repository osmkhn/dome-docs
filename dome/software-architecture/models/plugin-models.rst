Plugin Models
-------------------

Plugin models are any type of model where the executable simulation capabilities are provided by an external software application, such as a spreadsheet, CAD system, commercial simulation tools, analysis tools, optimization tools, or custom models written in a programming language. Several software programs are supported within DOME as plugin models. The analysis tools available within DOME are implemented as plugin models DOME models wrap the external simulation with DOME parameters that are connected to data elements within the external simulation. The two figures below provide a conceptual illustration of how DOME interacts with software possessing structured application program interfaces (APIs) and batch mode applications. 

.. image:: plugin-models/_static/dome-software-api.png

Figure 1: Connection of DOME parameters to software with a structured API library. This type of communication typically adds a few milliseconds (less than 5) per execution of the simulation in the external application.

.. image:: plugin-models/_static/dome-software-batch.png

Figure 2: Connection of DOME parameters to software that runs in batch mode. This type of interaction can add hundreds of milliseconds to seconds per execution of the simulation in the external application, depending upon how long it takes to read/write the input/output files.