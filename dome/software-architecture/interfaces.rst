Interfaces
-------------------

All types of models are executed through interfaces. Interfaces provide views that define which entities will be observable by users of the model at run-time. Model entities that are not in an interface are private and consequently will not be available to users.  Model builders can add parameters that are in a model to an interface. Further, all interfaces automatically contain filters to identify interface parameters that are independent inputs to and results from the model. Interfaces also contain a model view that allows anything within a model to be exposed to users in a read-only format.  Multiple interfaces can be associated with models to create different interfaces for different model users. The ability to define multiple interfaces for a model increases the reusability of a model.  The car example below provides a simple illustration of a model with multiple interfaces.

Car model (a flat list of entities)
Door length parameter
Overall length parameter
Wheel base parameter
Length relationship: Overall Length = 3 x Door Length
Car Context (a hierarchical visualization structure defined by the model builder)
Car Dimensions Context           
Door length parameter
Overall length parameter 
Wheel base parameter
Car Door Design Context
Door length parameter
Relationships Filtered Context
Length relationship
Door Designer Interface
Inputs Filtered Context (parameter values that can be changed by a user)
Door length parameter
Results Filtered Context
Overall length parameter
Body Designer Interface
Inputs Filtered Context (parameter values that can be changed by a user)
Wheel base parameter
Car Dimensions Context (parameters viewable by a user)
Door length parameter
Overall length parameter
Wheel base parameter

Access privileges defined during deployment determine what interfaces can be seen by different model users. All interfaces have support for builder-defined documentation. 