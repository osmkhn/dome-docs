Visualizations
-------------------

Visualizations are objects that accept data from parameters and transform them into a various graphical visualizations. For example, using a visualization object a matrix might be viewed as a elevation map, or two vectors might be plotted as an XY graph. All visualizations have support for builder-defined documentation. 