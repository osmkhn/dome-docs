Run Mode
-------------------

The Run Mode application is the browser of the World-Wide-Simulation-Web. It is used to surf between DOME servers and log into standalone models or collaborative playspace work areas. Parametric what-if scenarios or various types of analyses can be executed through the run application. 