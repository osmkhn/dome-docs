Build Mode
-------------------

The build authoring application allows users to create models, projects, and playspaces. The application provides mechanisms to find and subscribe to interface parameters of other models already deployed in the World-Wide-Simulation-Web.  When working in build mode the DOME models reside in one's local file space--not on a DOME server--and thus they are not available for use by others.