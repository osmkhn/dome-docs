Application Modes
=======================

DOME3 is a set of specialized applications that together provide a complete WWSW environment. Each application is focused for a different type of user or use. 

Build mode is an authoring application for model builders who define models and interfaces. 

Deploy mode is used to place models and interfaces on DOME servers. 

Run mode allows users to surf the WWSW and execute models. It is a browser application. 

Server mode is used to manage the users, groups, passwords and server file space

.. toctree::
   :titlesonly:

   build-mode.rst
   deploy-mode.rst
   run-mode.rst
   server-mode.rst
