Deploy Mode
-------------------

The Deploy Mode application is used to move models and playspaces to servers so that they are accessible to others in the World-Wide-Simulation Web. During the deployment process use and editing access privileges are defined. 