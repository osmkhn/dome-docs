Creating a Development Machine from a Local Machine
===================================================

.. contents:: :local:


Clone the DMC dev repo
-------------------------
::

  $ git clone https://bitbucket.org/DigitalMfgCommons/dmcdeploy_dev.git

Go to the sub folder:
::

  $ cd dmcdeploy_dev
 
Switch the branch to devmakina:
::

  $ git checkout devmakina 
 
Set Up AWS keys
---------------

Make the keys directory if not there:
::

  $ mkdir ~/keys

Create and download the AWS .pem key files you wish to use for your deployments:

* Key for the seed machine
* Key for dev machine
* Key for any other machines dev may launch

In the AWS EC2 console, under keypairs, create as many key pairs as needed and download them locally. By convention they are stored in ``~/keys``

If you are deploying from an AWS seed machine then you must upload to this seed machine the keys that you would like it to use when deploying subsequent machines.

Upload a .pem file to the seed machine. From your local machine (the machine you use to connect to AWS, not the AWS seed or dev machines):

::

  $ scp -i  seed.machine.key.pem remotekey.pem ec2-user@ip.of.seed.machine:/home/ec2-user/keys

(Note: "seedmachinekey.pem" is the name of the key from creating the instance in AWS and "remotekey" is a name you choose for the file and will use in the terraform.tfvars file below.)

Confirm that the remote .pem is on your dev machine by finding its name listed with the following command entered in the seed machine:
::

  ls ~/keys
 
Edit terraform.tfvars
---------------------
::

  $ cd dmcdeploy_dev/
  $ nano terraform.tfvars

* Input your aws credentials on the first two lines
* Edit key_name to the name of the remote key you just uploaded (without the .pem extension) 
* Edit the key_full_path to ``"~/keys/remote_key_name.pem"``

.. note::
  Make sure that the keys you have entered match the region where you wish to deploy to. Keys are only valid in the region they were created.
 
* Edit the stackPrefix  
* To launch the alpha release:
    * change release = "alpha"
* To launch latest nightly: 
    * change release = "hot"
* Edit the stackPrefix to something meaningful; this will be the prefix label for your new instances in AWS
* Edit the aws_region ('us-east-1' or 'us-west-2') as appropriate
 
Launch dev machine
------------------

* Uncomment the dev.tf file if needed by removing the comment characters on the first and last line (``nano dev.tf``)
* Comment out any other machines that may be there
 
Install Terraform if you have not already done so:
::

  $ cd /tmp
  $ wget https://releases.hashicorp.com/terraform/0.6.8/terraform_0.6.8_linux_amd64.zip

  $ mkdir ~/tf
  $ unzip terraform_0.6.8_linux_amd64.zip -d ~/tf
 
Make sure the path variables are properly set:
::

  $ echo "export PATH=$PATH:/home/ec2-user/tf" >> ~/.bashrc
 
Reload your bashrc:
::

  $ source ~/.bashrc

Verify install with:
::

  $ terraform -v 
 

Launch dev instance
-------------------

To create the plan in a file called "a" and check you are launching the infrastructure you think you are:
::

  $ terraform plan -out a

To launch that infrastructure from the plan created in the previous step:
::

  $ terraform apply a 

.. note::
  If something goes wrong, execute ``$ terraform destroy``. Then fix your configuration or terraform.tfvars file before repeating the terraform plan and terraform apply steps described above.

.. note::
  At this point your local machine or the Seed Machine will have deployed a Dev Machine on AWS. You will then use the Dev Machine to do further work.
 
SSH into dev instance
---------------------
To find out what the IP address of the dev machine is:
::

  $ terraform show

Read it from the line
::

  aws_instance.dev:
  ...
  ...
  public_ip = 52.23.214.212
  ...
  ...

Then:
::

  $ cd ~/keys
  $ chmod 400 <keyname>.pem    # in case amazon complains that key restrictions are too open
  $ ssh -i "key_used_in_terraformvars.pem" ec2-user@ip.of.dev.machine 

Accept the key fingerprint:
::

  $ yes

.. note::
  At this point you are logged into the Dev machine you have just created. The seed machine/ local laptop is no longer needed. 

To speed up your development you may now want to create a image of the dev machine in whatever region you want and launch that and use it for development thereby not needing to repeat many steps listed above. 


In the future a dev machine may be simply running and multiple developers will only need to ssh into it and not recreate it. 

As part of the security infrastructure developers interact with the DMC from the Dev Machines.

We want to be transparent with the tools and technology we use which is why we document and expose our processes instead of simply handing off opaque development environments.

The reason for using a seed machine is that this machine controls the setup of the dev machines. As the DMC is a rapidly evolving project the dev machines will need to be updated periodically. The seed machines can be used to give developers a personalized dev machine without those developers caring for/about the dev ops that created their instances.

Optional: switch to a better shell
----------------------------------
::
  
  $ zsh 
  $ export PATH=$PATH:/home/ec2-user/tf 
  $ alias tf='terraform'
