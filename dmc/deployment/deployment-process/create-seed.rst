Creating a Development Machine from a Seed Machine
==================================================

.. contents:: :local:

Option 1:  Use a prebuilt seed image
------------------------------------

Go to the EC2 Dashboard and launch a custom AMI image using the steps below.

For west:

West_Seed_1203:      ami-45a6b424  << linux machines

West_Seed_w_1203:    coming soon  << windows machines

For east:

East_Seed_1203:      ami-34f3be5e  << linux machine

East_Seed_w_1203:    coming soon  << windows machines

Steps to launch from custom image:

* EC2 Console > click launch instance.
* My AMI's > search for appropriate machine.
* Resize as appropriate (M4.large).
* Tag your instance with a memorable name.
* Choose the key-pair you wish and launch.

This instance will have the same configuration that you would get in Option 2 below.

Set up your private key on the machine from which you will access the Amazon instance. For PC users, PuTTY is a good tool to access the Amazon instance. If you are on a corporate network behind a firewall, consult with local administrators to determine how to access AWS instances. Configure PuTTY to use your private key by specifying the file in the PuTTY Configuration under Connection -> SSH -> Auth -> Private key file for authentication.

Log in to the new instance with ssh (putty). The username is ec2-user.

After logging in you can verify the configuration with:
::

  $ terraform -v
  $ git --version

Update your machine:
::

  $ sudo yum update -y

You can now follow the same instructions described in Creating a Development Machine from a Local Machine to create your personal dev machine that you will use to launch your stack or further develop.



Option 2:  Create your own seed machine
---------------------------------------

1. Create an AWS Linux machine following the AWS instructions. This machine can be in any region you want and of any size you choose.

2. SSH into the created machine.

3. Install the needed tools:

::

  $ sudo yum install -y git

Install Terraform:
::

  $ cd /tmp
  $ wget https://releases.hashicorp.com/terraform/0.6.8/terraform_0.6.8_linux_amd64.zip

  $ mkdir ~/tf
  $ unzip terraform_0.6.8_linux_amd64.zip -d ~/tf
 
4. Make sure the path variables are properly set:
::

  $ echo "export PATH=$PATH:/home/ec2-user/tf" >> ~/.bashrc
 
5. Reload your bashrc:
::

  $ source ~/.bashrc

6. Verify install with:
::

  $ terraform -v 
 
You can now follow the same instructions described in Creating a Development Machine from a Local Machine to create your personal dev machine that you will use to launch your stack or further develop.