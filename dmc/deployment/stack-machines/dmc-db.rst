DMC DB
=============

.. contents:: :local:

-------
Purpose
-------

Maintains information about the system in a Postgres/Oracle DB. 
 
--------
Postgres
--------
To access the database:

#. Switch to root after logging into the DB server, then switch to the postgres user using su postgres. 
#. Use the command psql to enter the Postgres command line. From here, you can navigate into the gforge database and view the database. 
#. Alternatively, enter the REST machine via ssh, and enter the command ``$ curl http://localhost:8080/rest/<mapping>``. This should return a JSON response if functioning.
#. If it does not, ensure the WAR file was unpackaged in /var/lib/tomcat7/webapps and that Tomcat is running. Otherwise, inspect catalina.out for errors. 
 
To check if postgres is running:
::

  netstat -ln | grep 5432

(This assumes you have configured postgres to use the default port of 5432)

Machine Type
------------
:Machine Type:      m4.xlarge
:AMI base image:      
:east:
:west:
 
Installed software:

* PostgreSQL 9.1
 
Depends On
----------
No other instances.

Dependents
----------
.. NOTE::
  These are based on a CloudFormation template. Some of these dependencies may no longer be true.

* SolrServer
* REST Services
* Shibboloeth
* Kerberos 

Ingress Rules
-------------
* Open port 5432 to SOLR, Rest Services, Kerberos/Shibboleth.  (This also used to be needed for ForgeFrontEnd.  Perhaps that is no longer the case, because the new deployment does not have a direct connection between the DB and the FrontEnd.)
* Open port 22 for SSH, Bastion only.

Egress Rules
------------
* Open port 5432 to SOLR, Rest Service, Kerberos/Shibboleth. 
* Ports 8000-8100 seem to be needed (perhaps specifically 8069). 
* Port 22 for SSH, Bastion only.

Ingress / Egress Testing Status
-------------------------------
* Tested by setting ports manually.
* Ingress rules tested using CloudFormation.

Required Vars
-------------
* Rest Server public IP
* Solr public IP Use Solr Server Private IP by adding entry to ``/var/lib/pgsql/9.1/data/pg_hba.conf``
* Example line added to pg_hba.conf

::

  host all gforge 172.31.34.148/32 trust

* kerberos/shibboleth public IP
* userPass
* adminPass
* DBPort (optional, otherwise use 5432)
 
Solr Search Integration
-----------------------
To enable Solr to pull data from the database configure the database to allow the SolR hsot to connect to the database.

For postgres: Add Solr Server Private IP to ``/var/lib/pgsql/9.1/data/pg_hba.conf``

Example line added to pg_hba.conf
::

  host all gforge 172.31.34.148/32 trust

------
Oracle
------
Oracle support is still in development. Please check back later for updated information.