DMC DOME
=============

.. contents:: :local:

Purpose
-------

The DOME Server allows for simulation execution.

Access & Testing
----------------

To test go to ``ipofdomeserver:8080/DOMEApiServicesV7/``
 
You should see a web page with the following list:

* Folders:
    * Fracture-Mechanics
        * Alpha
        * AppliedLoad
        * BetaFactor
        * BEVXOverP
        * CrackLength
        * DeltaK
        * Kmax
        * NormalCompliance
        * SigXCrack
        * SimulatedLoad
        * Ux
        * UxCompliance
        * X0OverWp2p8
        * X0OverWp8p1
    * File-Utilities
    * Mathematics
    * Physics

Machine Type
------------
:Machine Type:      m4.large
:AMI base image:      
:east:
:west:
 
Installed software:

* Dome Server ver. 7
* Tomcat Server ver. 7
* Java – OpenJDK Runtime Build 1.8.0_65 b17, OpenJDK 64-bit Server VM build 25.65-b01, mixed mode
* Git ver. 2.4.3 (Unix)
 
Depends On
----------
* ActiveMQServer

Dependents
----------
* ForgeFrontEnd 

Ingress Rules
-------------
Connections are from ActiveMQServer and ForgeFrontEnd 

* Port 8080, TCP.
* Port 7795, TCP.
* Port 22, TCP, for SSH, Bastion only.


* Open Ports– 61616 to talk to DMCActiveMq ??
* Open Ports – 80 to receive from DMCFrontEnd ??

Egress Rules
------------
* Assume same for now.

Ingress / Egress Testing Status
-------------------------------
* Not tested.

Required Environment Variables
------------------------------
* ActiveMqDns