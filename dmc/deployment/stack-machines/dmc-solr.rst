DMC Solr
========

.. contents:: :local:

Purpose
-------

Provides search for the DMC.

Access & Testing
----------------

To verify that the server is running correctly, follow these steps:

* Verify the administrative web interface is up at http://54.173.29.102:8983/solr/#/ (Substitute the IP address of your Solr instance instead of 54.173.29.102)
* Go to http://54.173.29.102:8983/solr/#/gforge_components/query
* Replace the q input with the following and press execute query: component_name:Physics
* Alternatively, copy this string into a browser: http://54.173.29.102:8983/solr/gforge_components/select?q=component_name:Physics&wt=json&indent=true

Output:
::

    {
      "responseHeader":{
        "status":0,
        "QTime":7,
        "params":{
          "q":"component_name:Physics",
          "indent":"true",
          "wt":"json"}},
      "response":{"numFound":1,"start":0,"docs":[
          {
            "group_id":"6",
            "component_name":"Physics",
            "id":"2",
            "group_name":"Low Heat Loss Transformer",
            "unix_group_name":"lowheatlosstran",
            "is_public":false,
            "_version_":1518308270913093632}]
    }}


* Go to http://54.173.29.102:8983/solr/#/gforge_projects/query
* Replace the q input with the following and press execute query: *%3A*
* Alternatively, copy this string into a browser: http://54.173.29.102:8983/solr/gforge_projects/select?q=*%3A*&wt=json&indent=true

Output: (no projects defined in database, so no results)
::

    {
      "responseHeader":{
        "status":0,
        "QTime":0,
        "params":{
          "q":"*:*",
          "indent":"true",
          "wt":"json"}},
      "response":{"numFound":0,"start":0,"docs":[]
      }}


* Go to http://54.173.29.102:8983/solr/#/gforge_services/query
* Replace the q input with the following and press execute query: group_name:Low*
* Alternatively, copy this string into a browser: http://54.173.29.102:8983/solr/gforge_services/select?q=group_name:Low*&wt=json&indent=true

Output:
::

    {
      "responseHeader":{
        "status":0,
        "QTime":11,
        "params":{
          "q":"group_name:Low*",
          "indent":"true",
          "wt":"json"}},
      "response":{"numFound":1,"start":0,"docs":[
          {
            "interface_data":"{\"interFace\":{\"version\":1,\"modelId\":\"995f865e-d90a-1004-8438-64281c6cab63\",\"interfaceId\":\"995f8660-d90a-1004-8438-64281c6cab63\",\"type\":\"interface\",\"name\":\"velocity interface\",\"path\":[33,34,35]},\"inParams\":{\"timeCopy\":{\"name\":\"timeCopy\",\"type\":\"Real\",\"unit\":\"second\",\"category\":\"time\",\"value\":2,\"parameterid\":\"0163d124-d8de-1004-8a2f-592d01a9bb93\"},\"distanceCopy\":{\"name\":\"distanceCopy\",\"type\":\"Real\",\"unit\":\"centimeter\",\"category\":\"length\",\"value\":3,\"parameterid\":\"0163d123-d8de-1004-8a2f-592d01a9bb93\"}},\"outParams\":{\"averageVelocity\":{\"name\":\"averageVelocity\",\"type\":\"Real\",\"unit\":\"centimeter per second\",\"category\":\"velocity\",\"value\":1.5,\"parameterid\":\"0163d125-d8de-1004-8a2f-592d01a9bb93\",\"instancename\":\"averageVelocity\"}},\"modelName\":\"velocity interface\",\"modelDescription\":\"\",\"server\":{\"name\":\"localhost\",\"port\":\"7795\",\"user\":\"ceed\",\"pw\":\"ceed\",\"space\":\"USER\"}}",
            "interface_name":"velocity interface",
            "id":"1",
            "server_id":"1",
            "cem_id":"2",
            "user_id":"102",
            "server_url":"http://ec2-52-88-73-23.us-west-2.compute.amazonaws.com:8080/DOMEApiServicesV7/",
            "group_id":"6",
            "group_name":"Low Heat Loss Transformer",
            "unix_group_name":"lowheatlosstran",
            "is_public":false,
            "_version_":1518308270970765312}]
      }}


* Go to http://54.173.29.102:8983/solr/#/gforge_users/query
* Replace the q input with the following and press execute query: user_name:berlier
* Alternatively, copy this string into a browser: http://54.173.29.102:8983/solr/gforge_users/select?q=user_name:berlier&wt=json&indent=true

Output:
::

    {
      "responseHeader":{
        "status":0,
        "QTime":1,
        "params":{
          "q":"user_name:berlier",
          "indent":"true",
          "wt":"json"}},
      "response":{"numFound":1,"start":0,"docs":[
          {
            "user_name":"berlier",
            "id":"103",
            "realname":"berlier test",
            "_version_":1518308898953494528}]
      }}


* Go to http://54.173.29.102:8983/solr/#/gforge_wiki/query
* Replace the q input with the following and press execute query: *%3A*
* Alternatively, copy this string into a browser: http://54.173.29.102:8983/solr/gforge_wikiselect?q=*%3A*&wt=json&indent=true

Output: (no wiki pages in database, so no results)
::

    {
      "responseHeader":{
        "status":0,
        "QTime":0,
        "params":{
          "q":"*:*",
          "indent":"true",
          "wt":"json"}},
      "response":{"numFound":0,"start":0,"docs":[]
      }}


Machine Type
------------
:Machine Type:      m4.large
:AMI base image:      
:east:
:west:
 
Installed software:

Solr software installed: 5.3.1.

The deployment script for Solr leverages the installation script provided by the Solr distribution. For a good description of the Solr installation process see `https://cwiki.apache.org/confluence/display/solr/Taking+Solr+to+Production <https://cwiki.apache.org/confluence/display/solr/Taking+Solr+to+Production>`_

The Solr script performs the following:

* Creates a user 'solr'
* Installs the Solr software in /opt/solr
* Configures Solr data (home) directory to: /var/solr/data
* Installs a startup script 

The deployment script performs these steps:

* Downloads gforge Solr configuration files from https://bitbucket.org/DigitalMfgCommons/dmcsolr.git
* Downloads Solr distribution from http://archive.apache.org/dist/lucene/solr/5.3.1/solr-5.3.1.tgz 
* Initiates the Solr installation script
* Unpacks the gforge Solr configuration files and copies them to /var/solr/data
* Restarts Solr
 
Depends On
----------
* DBBackEnd
* DMCDomeServer (?)

Dependents
----------
* DMCFrontEnd 

Ingress Rules
-------------
* Port 8983, tcp.
* Port 22 for open SSH, Bastion only.

Egress Rules
------------
* Assume same for now.

Ingress / Egress Testing Status
-------------------------------
Tested setting ports manually.

Ingress successfully tested using CloudFormation.

Required Environment Variables
------------------------------
* solrDbDns = DomeDbDns

Database Integration
--------------------

To enable Solr to pull data from the database configure the database to allow the Solr host to connect to the database.

For postgres: Add Solr Server Private IP to /var/lib/pgsql/9.1/data/pg_hba.conf

Example line added to pg_hba.conf: 
``host all gforge 172.31.34.148/32 trust``