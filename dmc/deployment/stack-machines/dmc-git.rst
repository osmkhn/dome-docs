DMC Git
=======

.. contents:: :local:

Purpose
-------

Git server available for DMC usage.

Access & Testing
----------------

Send git clone the demo repo found on the git server using
::

  ssh-agent bash -c 'ssh-add keytosshintogitserver.pem; git clone ec2-user@ipofgitserver:/tmp/test/my-project.git'

Machine Type
------------
:Machine Type:      m4.large
:AMI base image:      
:east:              ami-12663b7a – this is RHEL 7 (64)
:west:
 
Installed software:

* Git ver. latest (1.9.1)
 
Depends On
----------

None.

Dependents
----------

* DMC Front End

Ingress Rules
-------------
* Port 9418, tcp
* Port 443, tcp for ForgeFrontEnd
* Port 22, tcp, SSH, Bastion only.

Egress Rules
------------
* Assume same for now.

Ingress / Egress Testing Status
-------------------------------
* Not tested.

Required Environment Variables
------------------------------
None.