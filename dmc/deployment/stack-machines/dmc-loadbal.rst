DMC Load Balancer
=================

.. contents:: :local:

Purpose
-------

Load balance requests to the DMCFrontEnd.

Access & Testing
----------------

Open the Load Balancer DNS location and it should point you to the front page.

Machine Type
------------
:Machine Type:      AWS Load Balancer
 
Installed software:

* Git ver. latest (1.9.1)
 
Required Configuration
----------------------

* DMCFrontEnd machines

Connected To
------------

* DMC Front End


Ingress Rules
-------------


Egress Rules
------------


Ingress / Egress Testing Status
-------------------------------
