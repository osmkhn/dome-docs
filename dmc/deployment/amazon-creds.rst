Setting Up Amazon Credentials
=============================

To create a PEM file to login into your Amazon servers, please follow the steps in the following page: 
`http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair <http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair>`_
 
To create your public and private access keys:

* Visit the following page: `https://console.aws.amazon.com/iam/home?#users <https://console.aws.amazon.com/iam/home?#users>`_

* Find your Amazon login username. Select it and under the User Actions Dropdown at the top of the users list, select Manage Access Keys. Here you can create your access keys, both public and private.

.. warning::
    You can always visit the above link to get your public key, but this is the ONLY time you will be able to view your private key, so save it somewhere secure.
    
You will need these for several processes. 
 
Please refer to the following link on Amazon access key management:
`http://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html <http://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html>`_