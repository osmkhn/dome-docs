DMC REST Services Development
=============================

Pull down the latest code from git:
::

  git clone https://<username>@bitbucket.org/DigitalMfgCommons/restservices.git

Make any changes to the source code in the ``src/main/java`` branch. Tests are contained within ``src/test/java``.

Navigate to the directory of the ``pom.xml`` file. This is the build file that Maven uses. Make sure to include any dependencies not explicitly used by Java here (org.json, springboot-io, etc.)

Once you are happy with your code, you have some choices: 

1) You can start a local instance of your application with an embedded Tomcat container using ``mvn spring-boot:run``. This lives inside the shell until you kill the process. This process requires no more configuration. 


2) You can run your tests with ``mvn integration-test -P <profile listed in pom.xml>``. This will again start an embedded Tomcat container and run your tests against it. The Tomcat application will be terminated upon the end of your tests.

Results will be in the target/folder.


3) You can create a WAR file by running the command ``mvn package``. If successful, once complete, you will have a WAR file following your naming convention (set in ``pom.xml``) in the target/folder. 